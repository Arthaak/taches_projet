#!/usr/bin/env bash

# Basiquement une parodie de autoconf ; permet d'installer SDL sur les distros majeures (non-testé sur une autre qu'Ubuntu)

OS=`lsb_release -ds 2>/dev/null || cat /etc/*release 2>/dev/null | head -n1 || uname -om`

OS=$(echo $OS | cut -d ' ' -f1)

if [ -z `which sdl2-config` ]; then
	if [ $OS = "Ubuntu" ] || [ $OS = "Debian"]; then
		sudo apt install libsdl2-dev
		if [ ! -e /usr/local/include/SDL2/SDL_ttf.h]; then
			sudo apt install libsdl2-ttf-dev
		fi
		if [ ! -e /usr/local/include/SDL2/SDL_image.h]; then
			sudo apt install libsdl2-image-dev
		fi
		echo "Installation des dépendances complétée."
	elif [ $OS = "CentOS"] || [ $OS = "Fedora"]; then
		# tiré du wiki SDL2 - probablement excessif
		sudo yum install rpm-build alsa-lib-devel libX11-devel libXScrnSaver-devel libXau-devel libXcursor-devel libXext-devel libXfixes-devel libXi-devel libXinerama-devel libXrandr-devel libXrender-devel libXxf86vm-devel mesa-libGL-devel pulseaudio-libs-devel 
		sudo yum install SDL2-devel
		sudo yum install SDL2_image-devel
		sudo yum install SDL2_TTF-devel
		echo "Installation des dépendances complétée."
	else
		echo "Votre distribution n'est pas supportée, veuillez installer SDL manuellement."
	fi
else
	echo "Les dépendances sont déjà installées."
fi

path=`pwd`

desk_path="/usr/share/applications/rogue.desktop"
if [ -e $desk_path ]; then
	echo "Impossible de créer un raccourci bureau : un fichier sous ce nom existe déjà."
else
	sudo sh -c "echo '[Desktop Entry]
Version=1.0 
Type=Application
Terminal=false
Exec=${path}/bin/rogue
Name=Rogue
Comment=Projet de L2 info S2
Icon=${path}/assets/icon.ico
Categories=Game;' >> $desk_path"
	echo "Fichier de lancement 'rogue' créé."
fi



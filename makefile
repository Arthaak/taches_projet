CFLAGS_DEV= -ggdb3 -Wall -Iinclude
CFLAGS_PROD= -O3 -pipe -Iinclude -fexceptions

LINKFLAGS= `sdl2-config --cflags --libs` -lSDL2_ttf -lSDL2_image -lm
HEAD = $(wildcard include/*.h)

SRC = $(filter-out src/test.c, $(wildcard src/*.c))
SRC_TEST = $(filter-out src/main.c, $(wildcard src/*.c))

OBJ = $(SRC:src/%.c=obj/%.o)
OBJ_TEST = $(SRC_TEST:src/%.c=obj/%.o)


all: bin/rogue bin/test

bin/rogue: $(OBJ)
	gcc $(CFLAGS_DEV) $(OBJ) -o $@ $(LINKFLAGS)

bin/test: $(OBJ_TEST)
	gcc $(CFLAGS_PROD) $(OBJ_TEST) -o $@ $(LINKFLAGS) -lcunit

obj/%.o: src/%.c $(HEAD)
	@mkdir -p obj
	gcc $(CFLAGS_DEV) -c $< $(LINKFLAGS) -o $@

obj/test.o: src/test.c
	gcc $(CFLAGS_DEV) -c $< $(LINKFLAGS) -o $@ -lcunit

.PHONY: clean gdb valgrind


clean:
	rm $(wildcard obj/*.o)

gdb: 
	gdb --args bin/rogue

test:
	valgrind -v --leak-check=full bin/test

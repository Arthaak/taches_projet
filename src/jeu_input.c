#include "jeu_input.h"
#include "perso.h"
#include "monstre.h"
#include "jeu_interactions.h"


/** @file jeu_input.c
* Comporte les fonctions de traitement de l'input du joueur
*/

int coord_dans_rect(int x, int y, SDL_Rect RectCible) {
	return(x>RectCible.x && 
				 x<RectCible.x+RectCible.w &&
		 		 y>RectCible.y &&
				 y<RectCible.y+RectCible.h);
}


int moves_perso(int *timer, int orient, int xdelta, int ydelta, 
							 perso_t *perso, cell_t *cell[][M], 
							 monstre_t *lst_mstr, list_t **lst_projs) {

	if(perso->dir_orientation!=orient) {	
		*timer=SDL_GetTicks();
		perso->dir_orientation=orient;
		return -1;
	}
	else if(SDL_GetTicks()-*timer>60) {
		perso->x+=xdelta;
		perso->y+=ydelta;
		if (h_hors_limites(perso->y, perso->x) || 
		pj_collide(cell,perso, lst_mstr, lst_projs)) {
			perso->x-=xdelta;
			perso->y-=ydelta;
			return -1;
		}
		perso->mvt_timer=SDL_GetTicks();
	
		return(pj_marche_interac(perso, cell[perso->y][perso->x], cell));
	}
	return 0;
}


int input_kb(int *timer, perso_t *perso, cell_t *cell[][M], 
						 monstre_t* lst_mstr, list_t **lst_projs) {

	const Uint8* keystate=SDL_GetKeyboardState(NULL);
	
	if (keystate[SDL_SCANCODE_Q] && h_peut_attaquer(perso->arme)) {
		pj_tire_proj(perso);
		return 0;
	}
	if(perso->mvt_delay+perso->mvt_timer>SDL_GetTicks())
		return 0;
	
	if (keystate[SDL_SCANCODE_A]) {
		return moves_perso(timer, GAUCHE, -1, 0, perso,
											cell, lst_mstr, lst_projs);
	}
	else if (keystate[SDL_SCANCODE_D]) {
		return moves_perso(timer, DROITE, 1, 0, perso,
											cell, lst_mstr, lst_projs);
	}
	else if (keystate[SDL_SCANCODE_W]) {
		return moves_perso(timer, HAUT, 0, -1, perso, 
											cell, lst_mstr, lst_projs);
	}
	else if (keystate[SDL_SCANCODE_S]) {
		return moves_perso(timer, BAS, 0, 1, perso,
											cell, lst_mstr, lst_projs);
	}
	return 0;
}


void input_hud(SDL_MouseButtonEvent event, hud_t *interface) {
	
	if(event.button==SDL_BUTTON_LEFT) {
		if(event.y < HAUTEUR_HUD) {	
			if(coord_dans_rect(event.x,event.y,interface->pause))
				interface->stop=!(interface->stop);
			else if(coord_dans_rect(event.x,event.y,interface->quit)) {
				interface->loop=2; 
				interface->stop=1; // interrompt le cycle venant juste après le clic
			}
			else if(coord_dans_rect(event.x,event.y,interface->save))
				interface->saved=1;
		}
	}
}

void input_wrapper(SDL_Window *window, SDL_Event *event, hud_t *interface) {

	while(SDL_PollEvent(event)) {
		switch (event->type) { // ferme l'application
			case SDL_QUIT:
				interface->loop=3;
				break;
			case SDL_KEYDOWN:
				if(event->key.keysym.sym==SDLK_ESCAPE) 
					interface->loop=2;
				break;
			case SDL_MOUSEBUTTONDOWN: 
				input_hud(event->button,interface);			
			default:break; 
		}
	}
}


int input_end_screen(SDL_Rect RectQuit, SDL_Rect RectContinue, 
										 SDL_Rect RectBack) {
	SDL_Event event;
	if(SDL_WaitEvent(&event)) {
		switch (event.type) { // ferme l'application
			case SDL_QUIT: return 0;
			case SDL_KEYDOWN:
				if(event.key.keysym.sym==SDLK_ESCAPE)
					return 2;
				else if(event.key.keysym.sym==SDLK_RETURN)
					return 1;
			case SDL_MOUSEBUTTONDOWN:
				if(coord_dans_rect(event.button.x, event.button.y, RectQuit))
					return 0;
				else if(coord_dans_rect(event.button.x, event.button.y, RectContinue))
					return 1;
				else if(coord_dans_rect(event.button.x, event.button.y, RectBack))
					return 2;
			default: break;
		}
	}
	return -1;
}


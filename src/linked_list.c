#include <stdlib.h>
#include "core.h"
#include "linked_list.h"

/** @file linked_list.c 
* Implémentation basique d'une liste chaînée (possibilités : ajouter et supprimer un élément)
*/


void add_node(list_t **head, void *data) {
	if(!head)
		return;

	list_t *val=malloc(sizeof *val);
	val->data=data;
	val->next=(*head);
	
	(*head)=val;

}


void free_list(list_t **head, int free_data) {
	while(*head) {
		list_t *tmp=(*head);
		(*head)=(*head)->next;
		if(free_data)
			FREE(tmp->data);
		FREE(tmp);
	}
	FREE(*head); 
}

void remove_node(list_t **head, void** data) {
	if((*head) && (*head)->data==(*data)) {
		list_t *tmp=*head;
		(*head)=(*head)->next;
		FREE(*data);
		FREE(tmp);
		return;
	}
	
	list_t *curseur=(*head)->next;
	list_t *prev=(*head);
	
	while(curseur && prev) {
		if((*head) && (*head)->data==data) {
			list_t *tmp=curseur;
			prev->next=curseur->next;
			FREE(*data);
			FREE(tmp);
			return;
		}
		prev=curseur;
		curseur=curseur->next;
	}
	return;
	
}


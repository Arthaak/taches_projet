#include "jeu_interactions.h"
#include "jeu_architecture.h"
#include <SDL2/SDL.h>
#include "monstre.h"

/** @file jeu_interactions.c
* Comporte les diverses fonctions d'interaction génériques 
*/ 

int h_peut_attaquer(arme_t *arme) {
	return arme->timer+arme->delai<SDL_GetTicks();
}

int h_hors_limites(int x, int y) {
	return (x < 0 || y<0 || x>=N || y>=M);
}

int cell_bloquante(cell_t *cell) {
	return (cell->id==MUR || cell->id==TOUR || 
				 (cell->id==PORTE && cell->porte->statut==ACTIF));
}

int h_bresenham_search(int x1, int y1, int x2, int y2, cell_t *cell[][M]) {
	
	int distX=abs(x2-x1);
  int distY=abs(y2-y1); 
	int sx=x1<x2 ? 1 : -1;
  int sy=y1<y2 ? 1 : -1;
  int err=(distX>distY ? distX : -distY)/2;

	// Loop infini, pas dangereux en pratique car appelé uniquement dans des 
	// conditions où il s'achève
  while(1) {
   	if(x1==x2 && y1==y2)
   		return 1;

    if(err >-distX) { 
    	err-=distY; 
    	x1+=sx; 
    }
    if(err < distY) {
    	err+=distX;	
    	y1+=sy; 
    }

   	if(cell_bloquante(cell[x1][y1]))
   		return 0;
  }
}

int moves_hostile_projs(proj_t *proj, perso_t *perso, cell_t *cell[][M]) {

	if(proj->mvt_delay+proj->mvt_timer>SDL_GetTicks())
		return 0;
	if(proj->x==perso->x && proj->y==perso->y)
		return 2;

	int x1=proj->x;
	int y1=proj->y;
	
	int x2=proj->xdst;
	int y2=proj->ydst;
		
	int distX=abs(x2-x1);
  int distY=abs(y2-y1); 
	int sx=x1<x2 ? 1 : -1;
  int sy=y1<y2 ? 1 : -1;
  int err=(distX>distY ? distX : -distY)/2;	

	if(err >-distX) { 
		proj->x+=sx; 
	}
	// pas de else/return : on autorise le déplacement diagonal d'un projectile
	if(err < distY) { 
		proj->y+=sy; 
	}
	
	proj->mvt_timer=SDL_GetTicks();
	if(proj->x==perso->x && proj->y==perso->y)
		return 2;
	if(proj->x==proj->xdst && proj->y==proj->ydst) 
	 	return 1;
	if(cell_bloquante(cell[proj->y][proj->x]))
		return 1;
	 return 0;
}


void tour_attaque(perso_t *perso, cell_t *cell_tab[][M], 
											tour_t *tour, list_t **lst_projs) {
	if(cell_tab[tour->x][tour->y]->id!=TOUR)
		return;
	if(!h_peut_attaquer(tour->arme))
		return;
	if(tour->statut==INACTIF)
		return;

	int distX=abs(perso->y-tour->x); // inversion des x et y normale
	int distY=abs(perso->x-tour->y);

	// Comportement passif si le joueur n'est pas à portée
	// on n'utilise pas l'équation de la distance pour ne pas donner une case 
	// bonus en diagonale (portée de la tour en diamant plutôt qu'en carré)
	if(distX+distY>tour->arme->portee)
		return;
	

	// Vérification que le joueur n'est pas protégé par un obstacle (mur)
	// et attaque le cas échéant
	if(h_bresenham_search(tour->x, tour->y, perso->y, perso->x, cell_tab)
		&& !(tour->arme->projectile)) {
		tour->arme->projectile=projectile_init(tour->y, tour->x, 
																					 perso->x, perso->y, 250,
																					 tour->arme, lst_projs);
		tour->arme->timer=time(NULL);
	}

}


void tour_ia(perso_t *perso, cell_t *cell_tab[][M], 
								 list_t *lst_tours, list_t **lst_projs) {
	
	while(lst_tours) {
		tour_attaque(perso, cell_tab, lst_tours->data, lst_projs);
		lst_tours=lst_tours->next;
	}
}


void moves_projs(list_t **lst_projs, perso_t *perso,
												 cell_t *cell[][M], monstre_t *lst_mstr) {
	list_t *curseur=(lst_projs ? *lst_projs : NULL); 
	list_t *tmp;
	int check;
	while(curseur) {
		tmp=curseur->next;
		check=moves_hostile_projs(curseur->data, perso, cell);	

		if(check==2)
			pj_perd_vie(perso,((proj_t*)curseur->data)->degats);

		if(check==1 || check==2) {
			if(((proj_t*)curseur->data)->source) {
				((proj_t*)curseur->data)->source->projectile=NULL;
				((proj_t*)curseur->data)->source->timer=SDL_GetTicks();
			}
			remove_node(lst_projs, &(curseur->data));
		}

		curseur=tmp; // assure le déplacement sur la liste en cas de remove_node()
	}
	if(pj_move_proj(perso, cell, lst_mstr)==1) {
		FREE(perso->arme->projectile);
	}
}



#include "jeu_input.h"
#include "jeu_affichage.h"
#include "jeu_interactions.h"
#include "generic_SDL.h"

/** Prend le délai de déplacement x et calcule la vitesse de déplacement optimale de l'unité pour ne pas marquer de pause entre deux mouvements */
#define mvt_spd(x) upscaled(1000)/(FPS*(x))


jeu_tex_t init_textures(SDL_Renderer *rend) {

	jeu_tex_t compil;
	compil.tex_pj=create_texture("assets/pj.png", rend);
	compil.tex_tileset=create_texture("assets/tileset.png", rend);
	compil.tex_HUD=create_texture("assets/boutons.png", rend);

	for(int i=0;i<2;i++)
		for(int j=0;j<NB_SPRITES;j++)
			compil.rect_tile[i][j]=init_rect(j*40,i*40,40,40);
	
	
	for(int i=0;i<4;i++)
		for(int j=0;j<NB_PJ_ANIM;j++)
			compil.rect_pj[i][j]=init_rect(j*40,i*40,40,40);	
	
	for(int i=0;i<NB_HUD_ITEMS;i++)
		compil.rect_HUD[i]=init_rect(0,i*HUD_ITEM_SIZE,HUD_ITEM_SIZE,HUD_ITEM_SIZE);
	
	return compil;
}


int rect_est_visible(SDL_Rect RectSrc,SDL_Rect camera) {
	return (RectSrc.x+RectSrc.w >=0 &&
					RectSrc.y+RectSrc.h >=HAUTEUR_HUD &&
					RectSrc.x < camera.w &&
					RectSrc.y < camera.h+HAUTEUR_HUD);
}

int est_alt_tile(cell_t *cell) {
 return((cell->id==LEVIER && cell->levier->statut==INACTIF) ||
 				(cell->id==ARME && cell->arme->portee>1) ||
 				(cell->id==TOUR && cell->tour->statut==INACTIF));
}


int moves_display_rect(SDL_Rect *RectCible,int deplacement,SDL_Rect* RectModif) {

	int deltax=RectModif->x-RectCible->x;
	int deltay=RectModif->y-RectCible->y;

	if(!(deltax || deltay)) // rien à faire si les coordonnées sont les mêmes
		return 0;

	// Déplacement vers la gauche
	if(deltax <=SPRITE_SIZE && deltax > 0) {
		if(deltax <deplacement)		// gère les cas où SPRITE_SIZE % deplacement !=0
			RectModif->x=RectCible->x;
		else
			RectModif->x-=deplacement;	
		
	}

	// Déplacement vers la droite
	else if(deltax < 0 && deltax >=-SPRITE_SIZE) {
		if(deltax >-deplacement)
			RectModif->x=RectCible->x;
		else
			RectModif->x+=deplacement;	
	}
	// Téléportation en toute autre circonstance où la différence est non-nulle
	else if(deltax !=0)
		RectModif->x=RectCible->x;

	if(deltay <=SPRITE_SIZE && deltay > 0) {
		
		if(deltay < deplacement)
			RectModif->y=RectCible->y;
		else
			RectModif->y-=deplacement;	

	}
	else if(deltay < 0 && deltay >=-SPRITE_SIZE) {
		
		if(deltay >-deplacement)
			RectModif->y=RectCible->y;
		else
			RectModif->y+=deplacement;
	}	
	else if(deltay !=0)
		RectModif->y=RectCible->y;
		
	RectModif->w=RectCible->w;
	RectModif->h=RectCible->h;
	
	return deplacement;
}
int update_camera(perso_t *perso, SDL_Rect *camera) {

	SDL_Rect cam;

	cam.w=downscaled(LARGEUR_MAP);
	cam.h=downscaled(HAUTEUR_MAP);
	
	if(perso->x<cam.w/2)
		cam.x=0;
	else if(perso->x+cam.w/2>=M)
		cam.x=M-cam.w;
	else
		cam.x=perso->x-cam.w/2;
		
	if(perso->y<cam.h/2)
		cam.y=0;
	else if(perso->y+cam.h/2>=N)
		cam.y=N-cam.h;
	else
		cam.y=perso->y-cam.h/2;

	
	cam.y*=SPRITE_SIZE;
	cam.h*=SPRITE_SIZE;
	cam.x*=SPRITE_SIZE;
	cam.w*=SPRITE_SIZE;
	
	return moves_display_rect(&cam,mvt_spd(perso->mvt_delay),camera);
}	

	
void init_map_display(SDL_Rect camera,int *x1, int *x2, int *y1, int *y2) {
	
	*y1=camera.y;
	
	*y2=*y1 + camera.h;

	*x1=camera.x;

	*x2=*x1 + camera.w;
	
	*x1=floor(downscaled((double)*x1));
	*x2=ceil(downscaled((double)*x2	));
	*y1=floor(downscaled((double)*y1));
	*y2=ceil(downscaled((double)*y2));
}


int draw_perso(SDL_Renderer *rend, jeu_tex_t *src, 
							 perso_t *perso, hud_t* interface, int mvt) {


	SDL_Rect RectDest=init_rect(upscaled(perso->x)-interface->camera.x,
										 upscaled(perso->y)-interface->camera.y+HAUTEUR_HUD,
										 SPRITE_SIZE,SPRITE_SIZE);
	if(!rect_est_visible(RectDest, interface->camera))
		return 0;
	if(interface->camera.x%SPRITE_SIZE!=0)
		RectDest.x=perso->display.x;
	if(interface->camera.y%SPRITE_SIZE!=0)
		RectDest.y=perso->display.y;



	if(moves_display_rect(&RectDest, mvt_spd(perso->mvt_delay),
	&perso->display) || mvt) {
		perso->frame_count++;
		if(perso->frame_count==3)
			perso->frame_count=0;
	}
	
	SDL_RenderCopy(rend, src->tex_pj, 
								 &src->rect_pj[perso->dir_orientation][perso->frame_count], 
								 &perso->display);
	return 1;
}

/* Utilitaire pour draw_map() dessinant une cellule donnée selon son type */
void draw_cell(SDL_Renderer *rend, jeu_tex_t *src, cell_t *cell, int i, int j,
							 hud_t *interface) {
							
	SDL_Rect RectDest=init_rect(upscaled(j)-interface->camera.x,
											upscaled(i)-interface->camera.y+HAUTEUR_HUD,
											SPRITE_SIZE,SPRITE_SIZE);

	SDL_RenderCopy(rend,src->tex_tileset,
								&src->rect_tile[est_alt_tile(cell)][cell->id], &RectDest);
}

void draw_map(SDL_Renderer *rend,	jeu_tex_t *src, 
							cell_t *cell[][M], hud_t* interface) {

	int xdep, ydep, xdest, ydest;

	init_map_display(interface->camera, &xdep,&xdest,&ydep,&ydest);

	for(int i=ydep;i<ydest && i<N;i++)
		for(int j=xdep;j<xdest && j<M;j++) {
			draw_cell(rend, src,cell[i][j],i,j, interface);
		}		
}


int offset_camera(SDL_Rect *display, int offset, perso_t *perso) {
	if(!offset)
		return 0;
		
	if(perso->dir_orientation==HAUT)
		display->y+=offset;
	else if(perso->dir_orientation==BAS) 
		display->y-=offset;
	else if(perso->dir_orientation==GAUCHE)
		display->x+=offset;
	else if(perso->dir_orientation==DROITE)
		display->x-=offset;

	return 1;	
}




void draw_monstres(SDL_Renderer *rend, jeu_tex_t *src, monstre_t *lst_mstr, 
									 int offset, perso_t *perso, hud_t *interface) {
	SDL_Rect RectDest;
	for(int i=0;i<NB_MSTR;i++) {
		RectDest=init_rect(upscaled(lst_mstr[i].x)-interface->camera.x,
											 upscaled(lst_mstr[i].y)-interface->camera.y+HAUTEUR_HUD,
											 SPRITE_SIZE,SPRITE_SIZE);
		if(rect_est_visible(RectDest,interface->camera) ||
		rect_est_visible(lst_mstr[i].display,interface->camera)) {
	
			if(!offset_camera(&lst_mstr[i].display, offset,perso))
				moves_display_rect(&RectDest, 2+mvt_spd(lst_mstr[i].mvt_delay), 	
											 &lst_mstr[i].display);
	
			SDL_RenderCopy(rend,src->tex_tileset,&src->rect_tile[0][MONSTRE],
										 &lst_mstr[i].display);		
		}
	}
}


void draw_projs(SDL_Renderer *rend,	jeu_tex_t *src, list_t *lst_projs,
								int offset, perso_t *perso, hud_t *interface) {
	SDL_Rect RectDest;
	
	// Affichage des projectiles ennemis
	while(lst_projs) {

		proj_t *tmp=lst_projs->data;

		RectDest=init_rect(upscaled(tmp->x)-interface->camera.x,
											upscaled(tmp->y)-interface->camera.y+HAUTEUR_HUD,
											SPRITE_SIZE,SPRITE_SIZE);
		if(!offset_camera(&tmp->display, offset,perso))
			moves_display_rect(&RectDest, mvt_spd(tmp->mvt_delay), &tmp->display);

		SDL_RenderCopy(rend,src->tex_tileset,
									 &src->rect_tile[0][PROJ], &tmp->display);
		lst_projs=lst_projs->next;
	}
	
	// Affichage à part du projectile du personnage jouable
	if(perso->arme->projectile) {
		RectDest=init_rect(
				upscaled(perso->arme->projectile->x)-interface->camera.x,
				upscaled(perso->arme->projectile->y)-interface->camera.y+HAUTEUR_HUD,
				SPRITE_SIZE,SPRITE_SIZE);
		moves_display_rect(&RectDest, mvt_spd(perso->arme->projectile->mvt_delay),
											 &perso->arme->projectile->display);
		SDL_RenderCopy(rend,src->tex_tileset,&src->rect_tile[0][PROJ], 
									 &perso->arme->projectile->display);
	}
}


int draw_end(SDL_Renderer *rend, TTF_Font **font, int score,int statut,
						 int timer_debut, int nb_mstr) {

	SDL_Color color={255,255,255};
	
	SDL_RenderClear(rend);

	SDL_Rect RectMap=init_rect(0,0,LARGEUR_MAP,HAUTEUR_WIN);
	SDL_RenderFillRect(rend,&RectMap);
	
	char str[100];
	struct tm *temps;
	time_t timer_fin=time(NULL)-timer_debut;
	temps=localtime(&timer_fin);

	if(statut==0)
		strcpy(str, "Reposez en paix");
	else if(statut==1)
		strcpy(str, "Félicitations !");
	else if(statut==2)
		strcpy(str, "Réessayer ?");
		
		
	draw_text_at(rend,&color,font[2], LARGEUR_MAP/2-75,HAUTEUR_WIN/4,
							 str, 0);	
	
	snprintf(str,100, "Score : %i\nMonstres tués : %i sur %i\n"
									  "Temps écoulé : %i:%i\n", score, nb_mstr-NB_MSTR, nb_mstr,
					 																		temps->tm_min, temps->tm_sec);

	draw_text_at(rend,&color,font[1],LARGEUR_MAP/2-100,HAUTEUR_WIN/3,str,1000);

	SDL_Rect RectContinue=init_rect(LARGEUR_MAP/2-200,HAUTEUR_WIN*0.6,0,0);
	RectContinue=draw_text_in(rend,&color,font[1],RectContinue,
														"Recommencer\nune partie",260);

	SDL_Rect RectQuit=init_rect(LARGEUR_MAP/2+RectContinue.w/2,
															RectContinue.y+10,0,0);
	RectQuit=draw_text_in(rend,&color,font[1],RectQuit,"Quitter",0);
	
	SDL_Rect RectBack=init_rect((RectContinue.x+RectQuit.x)/2-50, RectContinue.y+50,0,0);
	RectBack=draw_text_in(rend,&color,font[1],RectBack, "Revenir à l'écran d'accueil",0);
	
	SDL_RenderPresent(rend);
	int choix;
	do {
		choix=input_end_screen(RectQuit, RectContinue, RectBack);
	} while(choix<0);

	
	return choix;
}


void HUD_init(hud_t *interface, perso_t *perso) {
	interface->loop=1;
	interface->saved=0;
	interface->stop=0;
	interface->vie=malloc(perso->vie*sizeof(SDL_Rect));

	interface->camera.w=LARGEUR_MAP;
	interface->camera.h=HAUTEUR_MAP;

	interface->camera.x=upscaled(perso->x)-interface->camera.w/2;
	if(interface->camera.x<0)
		interface->camera.x=0;

	interface->camera.y=upscaled(perso->y)-interface->camera.h/2;
	if(interface->camera.y<50)
		interface->camera.y=50;

	

}

void draw_HUD(SDL_Renderer *rend, jeu_tex_t *src,
							SDL_Color *color, TTF_Font **font, hud_t *interface,
							int maxhp, perso_t *perso) {
	

	// Nettoyage de ce qui a pu s'afficher dans la zone HUD
	interface->vie[0]=init_rect(0, 0,LARGEUR_MAP, HAUTEUR_HUD);
	SDL_RenderFillRect(rend,&interface->vie[0]);
	
	
	enum {SAVE,RESTART,PAUSE,QUIT,HPFULL,HPEMPTY};
	int i;
	for(i=0;i<maxhp;i++) {	
		interface->vie[i]=init_rect(10+(HUD_ITEM_SIZE+5)*i, 10,
												HUD_ITEM_SIZE, HUD_ITEM_SIZE);
		if(i<perso->vie)
			SDL_RenderCopy(rend,src->tex_HUD,
										 &src->rect_HUD[HPFULL], &interface->vie[i]);
		else
			SDL_RenderCopy(rend,src->tex_HUD,
										 &src->rect_HUD[HPEMPTY], &interface->vie[i]);
	}

	// Modifie le premier SDL_Rect de interface->vie pour englober l'ensemble du
	// tableau - facilite les calculs dans draw_popup()
	interface->vie[0]=init_rect(10,10, 10+(HUD_ITEM_SIZE+5)*maxhp,HUD_ITEM_SIZE);
	
	interface->quit=init_rect(LARGEUR_MAP-35, 10, HUD_ITEM_SIZE,HUD_ITEM_SIZE);
	interface->save=init_rect(interface->quit.x-HUD_ITEM_SIZE-5,
														interface->quit.y, HUD_ITEM_SIZE,HUD_ITEM_SIZE);
	interface->pause=init_rect(interface->save.x-HUD_ITEM_SIZE-5,
														 interface->save.y, HUD_ITEM_SIZE,HUD_ITEM_SIZE);
	interface->score=init_rect(interface->vie[i-1].x+50,14,0,0);
	interface->arme=init_rect(interface->score.x+100,0,0,0);
	interface->position=init_rect(interface->arme.x+100,15,0,0);
	interface->aide=init_rect(interface->position.x+200,10,0,0);
		
	char str[GUITXTLENGTH];
	snprintf(str,GUITXTLENGTH, "Dégâts : %i\nPortée : %i", perso->arme->degats,
																												 perso->arme->portee);
	interface->arme=draw_text_in(rend,color,font[1], interface->arme, str,90);

	snprintf(str,GUITXTLENGTH, "Score : %i", perso->score);	
	interface->score=draw_text_in(rend,color,font[1], interface->score, str,0);
	
	snprintf(str,GUITXTLENGTH, "Position : %i,%i", perso->x, perso->y);
	
	interface->position=draw_text_in(rend,color,font[1],interface->position, str,0);

	interface->aide=draw_text_in(rend,color,font[2],interface->aide, "?",0);


		
	SDL_RenderCopy(rend, src->tex_HUD,
								 &src->rect_HUD[(interface->stop ? PAUSE : RESTART)],
								 &interface->pause);
	SDL_RenderCopy(rend, src->tex_HUD,
								 &src->rect_HUD[SAVE], &interface->save);
	SDL_RenderCopy(rend, src->tex_HUD,
								 &src->rect_HUD[QUIT], &interface->quit);
	
}

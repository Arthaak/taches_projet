#include "menu.h"
#include "jeu.h"
#include "generic_SDL.h"
#include "core.h"
#include "jeu_input.h"

/** @file menu.c
* Comporte les fonctions qui gèrent toute la partie menu
*/


int menu(SDL_Window *window, TTF_Font **font, SDL_Renderer *render);

int recup_coord(SDL_Rect RectNew, SDL_Rect RectContinue, SDL_Rect RectQuit) {
	SDL_Event event;

	
	if(SDL_WaitEvent(&event)) {
		switch (event.type) { // ferme l'application
			case SDL_QUIT: return 2;
			case SDL_KEYDOWN:
				if(event.key.keysym.sym==SDLK_ESCAPE)
					return 2;
				else if(event.key.keysym.sym==SDLK_RETURN)
					return 1;
			case SDL_MOUSEBUTTONDOWN:
				if(coord_dans_rect(event.button.x, event.button.y, RectContinue))							//On récupère les coordonnées pour le bouton New, et on renvoie 0
					return 0;
				else if(coord_dans_rect(event.button.x, event.button.y, RectNew))			//O répère les coordonnées pour le bouton Continue et on renvoie 1
					return 1;
				else if(coord_dans_rect(event.button.x, event.button.y, RectQuit))					//idem
					return 2;
			default: break;
		}
	}
	return -1;
}


int recup_coord_lvl(SDL_Rect RectUn, SDL_Rect RectDeux, SDL_Rect RectTrois, SDL_Rect RectBack) {
	SDL_Event event;

	
	if(SDL_WaitEvent(&event)) {
		switch (event.type) { // ferme l'application
			case SDL_QUIT: return 0;
			case SDL_KEYDOWN:
				if(event.key.keysym.sym==SDLK_ESCAPE)
					return 0;
				else if(event.key.keysym.sym==SDLK_RETURN)
					return 1;
			case SDL_MOUSEBUTTONDOWN:
				if(coord_dans_rect(event.button.x, event.button.y, RectUn))							
					return 1;
				else if(coord_dans_rect(event.button.x, event.button.y, RectDeux))
					return 2;
				else if(coord_dans_rect(event.button.x, event.button.y, RectTrois))		
					return 3;
				else if(coord_dans_rect(event.button.x, event.button.y, RectBack))
					return 4;
			default: break;
		}
	}
	return -1;
}


int lvl_jeu(SDL_Window *window, TTF_Font **font, SDL_Renderer *render){

	SDL_Texture *menutext=create_texture("assets/menu2.png", render);

	SDL_Rect RectUn = init_rect(361,270,239,50);
	SDL_Rect RectDeux = init_rect(361,339,239,50);
	SDL_Rect RectTrois = init_rect(361,408,239,50);
	SDL_Rect RectBack = init_rect(35,606,170,37);


	int choix;

	
	/*FSOUND_Stream_Close(music);*/

	do {
		SDL_RenderCopy(render, menutext, NULL, NULL);
		SDL_RenderPresent(render);
		do{
			choix= recup_coord_lvl(RectUn, RectDeux,RectTrois,RectBack);
		}	while(choix==-1); 
		if(choix==4)
			return choix;
		choix=init_jeu(window, font, render, choix);

	} while(choix==1);
	

	
	SDL_DestroyTexture(menutext);

	return choix;
}




int menu(SDL_Window *window, TTF_Font **font, SDL_Renderer *render){


	SDL_Texture *menutext=create_texture("assets/menu.png", render);
	

	SDL_Rect RectNew=init_rect(361,270,239,50);
	SDL_Rect RectContinue = init_rect(361,339,239,50);
	SDL_Rect RectQuit = init_rect(361,408,239,50);

/*********************************************************************
	Partie à utiliser si on met une musique dans le menu
*********************************************************************/

/* 	FSOUND_STREAM *music = NULL;
	FSOUND_Init(44100,32,0) ; //Initialisation de la musique
	music = FSOUND_Stream_Open("nom_de_la_musique.ext", FSOUND_LOOP_NORMAL, 0,0);*/		//FSOUND_LOOP_NORMAL va jouer le son en boucle

/**********************************************************************
	Fin de la parie musique
**********************************************************************/

	//SDL_WM_SetIcon(SDL_LoadBMP("icon.bmp"),NULL);

	
	
	
			
/********************************************************************
	A enlever si on met une musique de fond
********************************************************************/

	/*FSOUND_StreamSetLoopCount(music, 1);  //Cette fonction sert à répeter la musique
	FSOUND_Stream_Play(FSOUND_FREE, music);*/ //Cette fonction va permettre de jouer la musique

	
	int choix, res_play;

	do {
		SDL_RenderCopy(render, menutext, NULL, NULL);
		SDL_RenderPresent(render);
		do{	
			choix= recup_coord(RectNew, RectContinue,RectQuit);
		}
		while(choix==-1); 	
		if(choix==0)
			res_play=init_jeu(window, font, render,0);
		if (choix == 1 || res_play==1)
			res_play=lvl_jeu(window,font,render);


	} while((res_play==2 || res_play==4) && choix !=2);
	/*FSOUND_Stream_Close(music);*/
	SDL_DestroyTexture(menutext);
	
	return res_play;
}









#include "jeu_architecture.h"
#include "jeu_interactions.h"
#include "generic_SDL.h"

/** @file jeu_architecture.c
* Comporte les fonctions d'initialisation des structures non-graphiques du jeu */

int NB_MSTR; 

void free_cell_type(cell_t* cell) { // helper
	if(cell)
		switch(cell->id) {
		case CLEF: FREE(cell->clef); break;
		case LEVIER: FREE(cell->levier); break;
		case ARME: FREE(cell->tour->arme->projectile); 
							 FREE(cell->arme); break;
		case TRESOR: FREE(cell->tresor); break;
		case TOUR: FREE(cell->tour->arme); 
							 FREE(cell->tour); break;	
		default: break;
		}
}

arme_t* init_arme(int degats, int portee, int delai) {
	arme_t *arme=malloc(sizeof *arme);
	arme->degats=degats;
	arme->portee=portee;
	arme->delai=delai;
	arme->timer=0;
	arme->projectile=NULL;
	return arme;
}


proj_t* projectile_init(int x1, int y1, int x2, int y2, int delai,
												arme_t *source, list_t **lst_projs) {
	proj_t *proj=malloc(sizeof *proj);
	proj->x=x1;
	proj->y=y1;
	proj->xdst=x2;
	proj->ydst=y2;
	proj->degats=source->degats;
	proj->mvt_delay=delai;
	proj->mvt_timer=SDL_GetTicks()+delai/3; // force une attente avant le mouvement 
	proj->source=source;	
	
	add_node(lst_projs, proj);
	
	return proj;
}



cell_t* cell_init(const int id, const int x, const int y, const int diff) {
	cell_t *cell=malloc(sizeof *cell);
	cell->id=id;
	
	switch(id) {
	case PORTE: cell_init_porte(cell); break;
	case ARME: cell_init_arme(cell); break;
	case TRESOR: cell_init_tresor(cell); break;
	case TOUR: cell_init_tour(cell, x, y, diff); break;
	default: break;	
	}
	return cell;	
}




void cell_init_clef(cell_t **cell, int x, int y) {
	*cell=malloc(sizeof **cell);
	(*cell)->id=CLEF;

	(*cell)->clef=malloc(sizeof(clef_t));
	(*cell)->clef->x_porte=x;
	(*cell)->clef->y_porte=y;
}


void cell_init_porte(cell_t *cell){
	cell->porte=malloc(sizeof(porte_t)); 
	cell->porte->statut = ACTIF;
}


void cell_init_arme(cell_t *cell) {
	cell->arme=malloc(sizeof(arme_t));
	cell->arme->degats=rand()%2+1;
	cell->arme->portee=rand()%4+1;
}


void cell_init_tresor(cell_t *cell) {
	cell->tresor=malloc(sizeof(tresor_t));
	cell->tresor->score=rand()%50;
}


void cell_init_tour(cell_t *cell, int x, int y, int difficulte) {
	cell->tour=malloc(sizeof(tour_t));
	cell->tour->y=y;
	cell->tour->x=x;
	cell->tour->arme=init_arme(rand()%difficulte*2+2,
														 5+4*(difficulte-1), 2500-(500*difficulte));
	cell->tour->statut=ACTIF;
}


void cell_init_levier(cell_t **cell, int x, int y) {
	(*cell)=malloc(sizeof *cell);
	(*cell)->id=LEVIER;
	
	(*cell)->levier=malloc(sizeof(levier_t));
	(*cell)->levier->y_tour=y;
	
	(*cell)->levier->x_tour=x;
	(*cell)->levier->statut=ACTIF;
}



void vide_cell(cell_t** cell, int delete) {	
	free_cell_type(*cell);
	if(delete) {
		FREE(*cell);
	}
	else
		(*cell)->id=VIDE;
}
	


void tours_init(cell_t *cell[][M], list_t **lst_tours) {
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++) {
				if(cell[i][j]->id==TOUR)
					add_node(lst_tours, cell[i][j]->tour);
		}
}


void map_free_mem(cell_t *cell[][M]) {
	for(int i=0;i<N;i++) 
		for(int j=0;j<M;j++) {
			vide_cell(&(cell[i][j]),1);
		}
}


#include "linked_list.h"
#include "jeu_architecture.h"
#include "perso.h"
#include "fonction_donjon.h"

#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>

// Test de l'implémentation des listes chaînées utilisée
void list_test(void)
{
	list_t *list=NULL;
	
	arme_t *datastruct=init_arme(15,21,5115);
	void **rm=(void**)&datastruct;
	
	add_node(&list, datastruct);
	CU_ASSERT(list!=NULL);
	CU_ASSERT(list->next==NULL);
	CU_ASSERT(list->data==datastruct);
	CU_ASSERT(((arme_t*)list->data)->degats==15);
	
	remove_node(&list, rm);	
	CU_ASSERT(list==NULL);
	
	add_node(&list,datastruct);
	add_node(&list,datastruct);
	add_node(&list,datastruct);
	CU_ASSERT(list->data==list->next->data);
	CU_ASSERT(list->data==list->next->next->data);

	free_list(&list, 1);
	CU_ASSERT(list==NULL);
}
 
// Test des fonctions d'interaction perso-map
void interac_cell_test(void) {

	perso_t *perso=pj_init(3);
	cell_t *cell=cell_init(TRESOR,0,0,0); // paramètres inutiles, ici
	
	int hp=perso->vie;
	perso->invul_delay=0; // nécessaire pour simuler une situation où du temps
	perso->invul_timer=1; // s'est écoulé depuis le début de la partie
	
	CU_ASSERT(perso->score==0);
	pj_marche_interac(perso, cell, NULL);
	CU_ASSERT(perso->score!=0);
	CU_ASSERT(cell->id==VIDE);
	
	pj_perd_vie(perso, 1);
	CU_ASSERT(perso->vie<hp);

	pj_free_mem(&perso);
	CU_ASSERT(perso==NULL);
	FREE(cell);	
}
// Test des fonctions d'interaction perso-unité hostile (tour/monstre)
void interac_hostiles_test(void) {

	// Positionnement des unités sur la map
	
	cell_t *cell[N][M];
	init_donjon(cell, 2);
	createDungeon_t(cell, 2);

	NB_MSTR=2;
	monstre_t *test=monstres_init(3, cell);
	perso_t *perso=pj_init(1);

	list_t *lst_tours=NULL;
	tours_init(cell, &lst_tours);
	
	// Test : création des tours
	CU_ASSERT(lst_tours->data==cell[((tour_t*)lst_tours->data)->x]
																 [((tour_t*)lst_tours->data)->y]->tour)
	
	free_list(&lst_tours,0);
		
	// Test : tir d'un projectile vers un monstre	
	
		// repositionnement du monstre
	vide_cell(&cell[perso->y][perso->x+1],0);
	test->x=perso->x+1;
	test->y=perso->y;
	CU_ASSERT(cell[test->y][test->x]->id==VIDE);

	int hp=test->vie;
	perso->dir_orientation=DROITE;

	pj_tire_proj(perso);
	perso->arme->projectile->mvt_delay=0;
	perso->arme->projectile->mvt_timer=1;
	
	CU_ASSERT(perso->arme->projectile!=NULL);
	moves_projs(NULL,perso, cell, test);
	CU_ASSERT(perso->arme->projectile==NULL);
		
	// Test : mort d'un monstre
	test[0].vie=0;
	pj_tuer_monstre(test,0,perso);
	CU_ASSERT(test[1].vie<hp);
	CU_ASSERT(NB_MSTR!=2);
	CU_ASSERT(perso->score!=0);
	
	// Test : libération de la mémoire allouée
	pj_free_mem(&perso);
	map_free_mem(cell);
	monstre_free_mem(&test, 2);	
	
	CU_ASSERT(test==NULL);
	
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)
			CU_ASSERT(cell[i][j]==NULL);
	
}


int main(void) {

	// nécessaire pour permettre les diverses comparaisons avec SDL_GetTicks()
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Delay(50);

  CU_initialize_registry();

  CU_pSuite suite_list = CU_add_suite("liste chaînée", NULL, NULL);
  CU_ADD_TEST(suite_list, list_test);

  CU_pSuite suite_interac = CU_add_suite("interactions", NULL, NULL);
  CU_ADD_TEST(suite_interac, interac_cell_test);
  CU_ADD_TEST(suite_interac, interac_hostiles_test);


  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
	
	SDL_Quit();
	return 0;
}





#include "monstre.h"


/** @file monstre.c
	Comporte toutes les fonctions impliquant en particulier les unités ennemies (dites "monstre")
*/


monstre_t monstre_init(int difficulte, cell_t *cell[][M]) {	
	monstre_t monstre;
	do {
		monstre.x=rand()%(M-5)+5;
		monstre.y=rand()%N;
	} while(h_hors_limites(monstre.y,monstre.x) || 
					cell_bloquante(cell[monstre.y][monstre.x]));
	monstre.vie=rand()%(2*difficulte)+2;
	monstre.arme=init_arme(rand()%difficulte+1,
												 rand()%difficulte*2+difficulte, 1000);
	monstre.dir_orientation=rand()%4;
	monstre.mvt_delay=1000-300*difficulte+rand()%300;
	monstre.mvt_timer=0;
	
	return monstre;
}

monstre_t* monstres_init(int difficulte, cell_t* cell[][M]) {

	monstre_t *lst_mstr=malloc(NB_MSTR*sizeof *lst_mstr);
	for(int i=0; i<NB_MSTR;i++) 
		lst_mstr[i]=monstre_init(difficulte, cell);
	return lst_mstr;
}


int monstre_peut_bouger(monstre_t monstre) {
	return monstre.mvt_delay+monstre.mvt_timer<SDL_GetTicks();
}

int monstre_collide(cell_t *cell[][M],int y, int x, monstre_t *lst_mstr) {
	if(cell[y][x]->id==MUR || cell[y][x]->id==TOUR) 
		return 1;
	for(int i=0;i<NB_MSTR;i++)
		if(lst_mstr[i].x==x && lst_mstr[i].y==y)
			return 1;
	return 0;
}

void monstre_free_mem(monstre_t **lst_mstr, int nb) {
	while(--nb>-1) {
		FREE((*lst_mstr)[nb].arme);
	}
	FREE(*lst_mstr);
}

int monstre_attaque(monstre_t *monstre,perso_t* perso, cell_t* cell[][M], 
										list_t **lst_projs) {
	
	if(!h_peut_attaquer(monstre->arme))
		return 1; // garde l'attention du mob sur le PJ
			
	int check;		
			
	switch(monstre->dir_orientation) {
		case BAS:
			for(int i=1;i<=monstre->arme->portee && monstre->y+i<N;i++) {
				check=monstre_tire_proj(0, i, monstre, perso, cell, lst_projs);
				if(check==1)
					return 1;
				else if(check==2)
					break;
			}
			break;
		case HAUT:
			for(int i=1;i<=monstre->arme->portee && monstre->y-i>=0;i++) {
				check=monstre_tire_proj(0, -i, monstre, perso, cell, lst_projs);
				if(check==1)
					return 1;
				else if(check==2)
					break;
			}
			break;
		case GAUCHE:
			for(int i=1;i<=monstre->arme->portee && monstre->x-i>=0;i++) {
				check=monstre_tire_proj(-i, 0, monstre, perso, cell, lst_projs);
				if(check==1)
					return 1;
				else if(check==2)
					break;
			}
			break;
		case DROITE:
			for(int i=1;i<=monstre->arme->portee && monstre->x+i<M;i++) {
				check=monstre_tire_proj(i, 0, monstre, perso, cell, lst_projs);
					if(check==1)
						return 1;
					else if(check==2)
						break;
			}
			break;
	}
	return 0;
}

void monstre_ia(monstre_t *lst_mstr, int i, perso_t *perso,
										cell_t *cell[][M], list_t **lst_projs) {

	// interdit un tir avant même d'arriver (au niveau visuel) sur une case
	if(!monstre_peut_bouger(lst_mstr[i]))
		return;
		
	if(monstre_attaque(&lst_mstr[i], perso, cell, lst_projs) || lst_mstr[i].arme->projectile)	
		return;
			 
	int k=rand();	
	if(k<RAND_MAX*0.12)
		return;	
	
	else if(k<RAND_MAX*0.34 && lst_mstr[i].x<M-1 && 
	!monstre_collide(cell,lst_mstr[i].y,lst_mstr[i].x+1,lst_mstr))
		monstre_moves(DROITE, 1, 0, i, lst_mstr, perso, cell, lst_projs);

	else if(k<RAND_MAX*0.56 && lst_mstr[i].x>0 && 
	!monstre_collide(cell,lst_mstr[i].y,lst_mstr[i].x-1,lst_mstr))
		monstre_moves(GAUCHE, -1, 0, i, lst_mstr, perso, cell, lst_projs);

	else if(k<RAND_MAX*0.78 && lst_mstr[i].y<N-1 && 
	!monstre_collide(cell,lst_mstr[i].y+1,lst_mstr[i].x, lst_mstr))
		monstre_moves(BAS, 0, 1, i, lst_mstr, perso, cell, lst_projs);

	else if(k<RAND_MAX			&& lst_mstr[i].y>0 && 
	!monstre_collide(cell,lst_mstr[i].y-1,lst_mstr[i].x, lst_mstr))
		monstre_moves(HAUT, 0, -1, i, lst_mstr, perso, cell, lst_projs);
}

void monstres_ia(monstre_t *lst_mstr, perso_t *perso,
										cell_t *cell[][M], list_t **lst_projs) {
	for(int i=0;i<NB_MSTR;i++)
		monstre_ia(lst_mstr, i,perso, cell, lst_projs);
}

void monstre_moves(int orient, int xdelta, int ydelta, int i, 
											 monstre_t *lst_mstr, perso_t *perso,
											 cell_t *cell[][M], list_t **lst_projs) {

	lst_mstr[i].dir_orientation=orient;
	if(!monstre_attaque(&lst_mstr[i], perso, cell, lst_projs)) {
		lst_mstr[i].x+=xdelta;
		lst_mstr[i].y+=ydelta;
		lst_mstr[i].mvt_timer=SDL_GetTicks();
		}
}

int monstre_tire_proj(int xdelta, int ydelta, monstre_t *monstre,
											 perso_t* perso,cell_t* cell[][M], list_t **lst_projs) {
													 
	if(cell_bloquante(cell[monstre->y+ydelta][monstre->x+xdelta]))
		return 2;
		
	else if(perso->x==monstre->x+xdelta && monstre->y+ydelta==perso->y) {
		if(!monstre->arme->projectile) {
			monstre->arme->projectile=projectile_init(monstre->x, monstre->y, 
																								perso->x, perso->y, 150,
																								monstre->arme, lst_projs);
			return 1;
		}
	} 	
	return 0;
}



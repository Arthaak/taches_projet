#include "jeu_architecture.h"
#include "monstre.h"

/** @file perso.c
	Comporte toutes les fonctions impliquant particulièrement le personnage jouable
*/


perso_t* pj_init(int difficulte) {
	// Valeurs semi-arbitraires visant à rendre le niveau 1 de difficulté
	// enfantin, le 2 faisable et le 3 quasiment impossible
	
	perso_t *perso=malloc(sizeof *perso);
	perso->vie=7-2*difficulte;
	perso->x=0;
	perso->y=N/2; // x,y définis par la création de la map
	perso->dir_orientation=DROITE;
	perso->mvt_delay=200;
	perso->mvt_timer=0;
	perso->arme=init_arme(6-2*difficulte, 13-4*difficulte, difficulte*200);
	perso->arme->delai=400;
	perso->score=0;
	perso->frame_count=0;
	perso->invul_timer=500;
	perso->invul_delay=75+250*(4-difficulte);
	return perso;
}

int pj_perd_vie(perso_t *perso, int degats) {
	if(perso->invul_delay+perso->invul_timer<SDL_GetTicks()) {
		perso->invul_timer=SDL_GetTicks();
		if(perso->vie>1 && perso->vie-degats<1) // empêche un pur one-shot
			perso->vie=1;													// en difficulté intermédiaire
		else
			perso->vie-=degats;
		return 1;
	}
	return 0;
}


void pj_tuer_monstre(monstre_t *lst_mstr, int i, perso_t *perso) {
	if(lst_mstr[i].vie<=0) {
		for(;i<NB_MSTR-1;i++) {
			monstre_t tmp=lst_mstr[i];
			lst_mstr[i]=lst_mstr[i+1];
			lst_mstr[i+1]=tmp;
		}
		NB_MSTR--;
		perso->score+=rand()%20+30;
	}	
}


int pj_augmente_score(perso_t *perso, cell_t *cell) {
	if(cell->id==TRESOR) {
		perso->score+=cell->tresor->score;
		vide_cell(&cell,0);
		return TRESOR;
	}
	return 0;

}

int pj_ouvre_porte(cell_t *cell, cell_t *cell_tab[][M]) {
	if(cell->id==CLEF) { 
		cell_tab[cell->clef->x_porte][cell->clef->y_porte]->porte->statut = INACTIF;
		vide_cell(&cell,0);
		return CLEF;
		}
	return 0;
}
	
int pj_passe_porte(cell_t *cell) {
	if(cell->id==PORTE && cell->porte->statut==INACTIF) {
		return PORTE;
		}
	return 0;
}

int pj_collide(cell_t *cell[][M], perso_t *perso, monstre_t *lst_mstr, 
							 list_t **lst_projs) {
	if(cell_bloquante(cell[perso->y][perso->x]))
		return 1;
	for(int i=0;i<NB_MSTR;i++)
		if(lst_mstr[i].x==perso->x && lst_mstr[i].y==perso->y)
			return (pj_perd_vie(perso,1) || 1);
		// légèrement hacky, mais simplifie la rédaction : retire 1 HP et renvoie 1
		// si le PJ percute un monstre
		
			
	list_t *curseur=*lst_projs;
	while(curseur) {
		if(((proj_t*)curseur->data)->x==perso->x && 
		((proj_t*)curseur->data)->y==perso->y) {
		
			pj_perd_vie(perso,((proj_t*)curseur->data)->degats);
		
			((proj_t*)curseur->data)->source->projectile=NULL;
			((proj_t*)curseur->data)->source->timer=SDL_GetTicks();
		
			remove_node(lst_projs, &(curseur->data));
		
			return 0;
		}
		curseur=curseur->next;
	}
	return 0;
}

int pj_attaque_mstr(perso_t *perso,	monstre_t *lst_mstr) {
	for(int i=0;i<NB_MSTR;i++)
		if(lst_mstr[i].x==perso->arme->projectile->x && 
		lst_mstr[i].y==perso->arme->projectile->y) {
			lst_mstr[i].vie-=perso->arme->projectile->degats;
			if(lst_mstr[i].vie<1)
				pj_tuer_monstre(lst_mstr, i, perso);
			return 1;
			}
	return 0;
}

void pj_tire_proj(perso_t* perso) {	
	if(!perso->arme->projectile)
		switch(perso->dir_orientation) {
			case GAUCHE:
				perso->arme->projectile=projectile_init(perso->x-1, perso->y, 
																							perso->x-perso->arme->portee, 
																							perso->y, 100, perso->arme, NULL);
				break;
				case DROITE:
				perso->arme->projectile=projectile_init(perso->x+1, perso->y, 
																							perso->x+perso->arme->portee,
																							perso->y, 100, perso->arme, NULL);
				break;
				case HAUT:
				perso->arme->projectile=projectile_init(perso->x, perso->y-1, perso->x,
																							perso->y-perso->arme->portee, 100,
																							perso->arme, NULL);
				break;
				case BAS:
				perso->arme->projectile=projectile_init(perso->x, perso->y+1, perso->x,
																							perso->y+perso->arme->portee, 100,
																							perso->arme, NULL);
				break;
		}
}

int pj_move_proj(perso_t *perso, cell_t *cell[][M], monstre_t* lst_mstr) {
	if(!perso->arme->projectile)
		return 0;
	
	// Le projectile apparaît une case à côté du perso et non directement
	// sur celui-ci : on fait donc une batterie de vérifications qu'il peut 
	// exister avant de tenter de le déplacer
		
	if(h_hors_limites(perso->arme->projectile->y,
										perso->arme->projectile->x))
		return 1;

	if(cell_bloquante(cell[perso->arme->projectile->y]
												[perso->arme->projectile->x]))
		return 1;	
	
	if(perso->arme->projectile->mvt_delay+
	perso->arme->projectile->mvt_timer>SDL_GetTicks())
		return 0;

	// On vérifie avant et après qu'aucun monstre n'est sur la même case
	// puisqu'un monstre peut s'y déplacer pendant la latence du projectile
	if(pj_attaque_mstr(perso, lst_mstr))
		return 1;

	if(perso->arme->projectile->x==perso->arme->projectile->xdst &&
	perso->arme->projectile->y==perso->arme->projectile->ydst) 
	 	return 1;

	// Algorithme de Bresenham. Techniquement excessif puisque le PJ ne peut 
	// tirer qu'en ligne droite, mais simplifie la potentielle 
	// implémentation des tirs en diagonale	

	int x1=perso->arme->projectile->x;
	int y1=perso->arme->projectile->y;

	int x2=perso->arme->projectile->xdst;
	int y2=perso->arme->projectile->ydst;

	int distX=abs(x2-x1);
	int distY=abs(y2-y1); 
	int sx=x1<x2 ? 1 : -1;
	int sy=y1<y2 ? 1 : -1;
	int err=(distX>distY ? distX : -distY)/2;	

	if(err >-distX) { 
		perso->arme->projectile->x+=sx; 
	}
	if(err < distY) { 
		perso->arme->projectile->y+=sy; 
	}
	
	perso->arme->projectile->mvt_timer=SDL_GetTicks();
	
	if(pj_attaque_mstr(perso, lst_mstr))
		return 1;
	
	
	
	return 0;

}

int pj_marche_levier(cell_t *cell, cell_t *cell_tab[][M]){
	if(cell->id == LEVIER){
		cell->levier->statut = INACTIF;
		cell_tab[cell->levier->x_tour][cell->levier->y_tour]->tour->statut=INACTIF;
		return LEVIER;
	}
	return 0;
}

int pj_marche_interac(perso_t* perso, cell_t* cell, 
											cell_t *cell_tab[][M]) {
	switch(cell->id) {
		case CLEF:	
			return (pj_ouvre_porte(cell, cell_tab));
		case TRESOR:
			return (pj_augmente_score(perso, cell));
		case LEVIER:
			return(pj_marche_levier(cell, cell_tab));	
		case PORTE:
			return(pj_passe_porte(cell));	
		default: return 0;
	}
}


void pj_free_mem(perso_t **perso) {
	FREE((*perso)->arme->projectile);
	FREE((*perso)->arme);
	FREE(*perso);
}





#include "generic_SDL.h"
#include "core.h"
#include <SDL_image.h>
#include <stdlib.h>
#include <unistd.h>


/** @file generic_SDL.c
* Comporte les fonctions d'initialisation de SDL ainsi que les fonctions utilitaires non-spécifiques au jeu proprement dit.
*/


void init_SDL(SDL_Window** window, TTF_Font** font) {

	if (SDL_Init(SDL_INIT_VIDEO) != 0 || TTF_Init()<0) {
		fprintf(stdout,"Échec de l'initialisation de la SDL (%s)\n",SDL_GetError());
		exit(0);
	}
	*window = SDL_CreateWindow("Rogue", SDL_WINDOWPOS_UNDEFINED, 
														SDL_WINDOWPOS_UNDEFINED, 
														LARGEUR_MAP,HAUTEUR_WIN,0);


	if (!window) {
		fprintf(stderr,"Erreur de création de la fenêtre: %s\n",SDL_GetError());
		SDL_Quit();
	}

	SDL_Surface *icon=IMG_Load("../assets/icon.ico");
	SDL_SetWindowIcon(*window, icon);
	SDL_FreeSurface(icon);

	
	for(int i=0;i<NB_FONTS;i++) 
		font[i]=TTF_OpenFont("../assets/arial.ttf", 12+6*i);
	
	if(!(*font)) {
		fprintf(stderr, "Erreur de création de la police d'écriture : %s\n", 
						TTF_GetError());
		exit(0);
	}
}

SDL_Renderer* init_render(SDL_Window *window) {

	SDL_Renderer *rend=SDL_CreateRenderer(window,-1, SDL_RENDERER_ACCELERATED);
	if(!rend) {
		SDL_DestroyWindow(window);
		fprintf(stderr,"Erreur de création du renderer : %s\n",SDL_GetError());
		SDL_Quit();
	}

	return rend;
}


SDL_Rect init_rect(int x, int y, int w, int h) {
	SDL_Rect rect={x,y,w,h};
	return rect;
}


SDL_Rect draw_text_in(SDL_Renderer* rend, SDL_Color *color, TTF_Font *font,
							 SDL_Rect RectSrc, char *str, int wrapped) {
							 
	SDL_Surface *txt_s;
	
	if(wrapped)
		txt_s=TTF_RenderUTF8_Blended_Wrapped(font, str,*color, wrapped);
	else
		txt_s=TTF_RenderUTF8_Blended(font, str,*color);

	SDL_Texture *texture=SDL_CreateTextureFromSurface(rend, txt_s);

	int width,height;
	SDL_QueryTexture(texture, NULL,NULL,&width, &height);
	RectSrc=init_rect(RectSrc.x, RectSrc.y, width,height);

	SDL_RenderCopy(rend, texture, NULL, &RectSrc);
	
	SDL_FreeSurface(txt_s);
	SDL_DestroyTexture(texture);
	
	return RectSrc;
}


SDL_Rect draw_text_at(SDL_Renderer* rend, SDL_Color *color, TTF_Font *font,
							 int x,int y, char *str, int wrapped) {
	SDL_Rect rect=init_rect(x,y,0,0); // w et h sont écrasés dans draw_text_in()
	return draw_text_in(rend, color, font, rect, str, wrapped);
}	


SDL_Texture* create_texture(char *path, SDL_Renderer *rend) {

	char str[200];
	sprintf(str,"../%s",path);

	SDL_Surface *sfc=IMG_Load(str);
	
	if(!sfc)
		perror("Impossible de créer la surface");
	
 	Uint32 colorkey = SDL_MapRGB(sfc->format,0,255,255);
	SDL_SetColorKey(sfc, SDL_TRUE, colorkey);
	
	SDL_Texture *tex=SDL_CreateTextureFromSurface(rend, sfc);
	SDL_FreeSurface(sfc);

	return tex;
}	


void change_to_cwd(void) {

	char str[200];

	// récupère le chemin absolu de l'exécutable, retire le nom dudit exécutable
	// et se place dans le dossier
	#ifdef _WIN32 // non-testé
		GetModuleFileName(NULL, str, 200);
	#else
		readlink("/proc/self/exe", str, 200);
	#endif

	char *buffer=strrchr(str, '/');

	buffer[1]='\0';
	chdir(str);
}



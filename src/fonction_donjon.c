#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "core.h"
#include "jeu_architecture.h"
#include "fonction_donjon.h"

int cpt_time = 0; 

int yprov;
int xprov;


void init_donjon(cell_t * d[N][M], int difficulte){
	int i;
	int j;
	
	for(i = 0; i < N; i++){
		for(j = 0; j< M; j++){
			d[i][j]  = cell_init(MUR,i,j, difficulte);
		}
	}
}

void texture(cell_t * d[N][M], int x, int y, int celltype, int difficulte)
{
		vide_cell(&(d[x][y]),1);
	  d[x][y] = cell_init(celltype,x,y, difficulte);
}

int nbr_aleatoire(int min, int max)
{
	if(cpt_time == 0){
		srand(time(NULL)); /*Initialisation de rand*/
		cpt_time++;
	}	
	int nbr_al_tmp, nbr_al;
	nbr_al_tmp=rand()%20000;
	nbr_al = nbr_al_tmp%(max - min) + min;
	return nbr_al;
}


int makeCorridor(cell_t * d[N][M], int minlength, int maxlenght, 
								 int direction, int difficulte)
{
	int len = nbr_aleatoire(minlength,maxlenght); /*Renvoie la taille du couloir*/
	
	int floor = VIDE;
	
	int dir;
	if(direction >= 0 && direction <=3){
		dir = direction;
	}
	
	int i;
	
	switch(dir){
    case 0:
			{
				for(i = xprov; i < xprov + len && i <= N-1; i++){
					texture(d, i, yprov, floor, difficulte);
					
				}
				
				if(xprov+len > N-1){
					xprov = N-1;
				}
				else{
					xprov = xprov+len;
				}

				break;
			}
    case 1:
			{
				for(i = yprov; i < yprov + len && i <= M-1; i++){
					texture(d, xprov, i, floor, difficulte);
					
				}
				if(yprov+len > M-1){
					yprov = M-1;
				}
				else{
					yprov = yprov+len;
				}
				
				break;
			}
		case 2:
			{
				for(i = xprov; i > xprov - len && i >=0; i--){
					texture(d, i, yprov, floor, difficulte);	
				}
				if(xprov-len < 0 ){
					xprov = 0;
				}
				else{
					xprov = xprov-len;
				}
				break;
			}
		case 3:
			{
				for(i = yprov; i > yprov - len && i >=0; i--){
					texture(d, xprov, i, floor, difficulte);
					
				}
				if(yprov-len < 0 ){
					yprov = 0;
				}
				else{
					yprov = yprov-len;
				}
				break;
			}									
	}
	return dir;
}

int opposed(int direction, int choix){ /*Renvoie 0 si un choix de couloir est opposé à la direction actuelle, 1 sinon*/
	if(direction == 0 && choix == 2)
		return 0;
	if(direction == 1 && choix == 3)
		return 0;
	if(direction == 2 && choix == 0)
		return 0;
	if(direction == 3 && choix == 1)
		return 0;			
	return 1;
}

int choix_couloir(int direction){
	int dir;
	if(yprov == 0){
		if(xprov == 0){
			do{
				dir = nbr_aleatoire(0,4);
			}while((dir == 3 || dir == 2) && (opposed(direction, dir) != 1));
			return dir;
		}
		else{
			do{
				dir = nbr_aleatoire(0,4);
			}while(dir == 3 && opposed(direction, dir) != 1);
			return dir;			
		}
	}
	else{
		if(xprov == 0){
			do{
				dir = nbr_aleatoire(0,4);
			}while(dir == 2 && opposed(direction, dir) != 1);
			return dir;
		}
		do{				
			dir = nbr_aleatoire(0,4);
		}while(opposed(direction, dir) != 1);
		return dir;	
	}
}

int nbr_mur(cell_t * d[N][M]){
	int i;
	int j;
	int compteur = 0;
	for(i = 0; i<N; i++){
		for(j = 0; j<M; j++){
			if(d[i][j]->id == MUR){
			compteur++;
			}
		}
	}
	return compteur;
}

int keygen(cell_t * d[N][M], int x_porte, int difficulte){
	int i;
	int j;
	int abs_dep;
	if(x_porte <= N/2){
		abs_dep = nbr_aleatoire(N/2, N-1);
		for(i = abs_dep; i > 0; i-- ){
			for(j = M - 18; j > 18; j--){
				if(d[i][j]->id == VIDE){
					cell_init_clef(&d[i][j], x_porte,M - 1);
					return 0;
				}
			}
		}
	}
	else{
		abs_dep = nbr_aleatoire(0, N/2);
		for(i = abs_dep; i < N -1; i++ ){
			for(j = M - 18; j > 18; j--){
				if(d[i][j]->id == VIDE){
					cell_init_clef(&d[i][j], x_porte,M - 1);
					return 0;
				}
			}
		}		
	}
	return 1;
}

int chestgen(cell_t * d[N][M], int x_porte, int difficulte){
	int i;
	int j;
	int abs_dep;
	if(x_porte <= N/2){
		abs_dep = nbr_aleatoire(0, N/2);
		for(i = abs_dep; i < N -1; i++ ){
			for(j = M - 12; j > 12; j--){
				if(d[i][j]->id == VIDE){
					texture(d,i,j,TRESOR, difficulte);
					return 0;
				}
			}
		}
	}
	else{
		abs_dep = nbr_aleatoire(N/2, N-1);
		for(i = abs_dep; i > 0; i-- ){
			for(j = M - 12; j > 12; j--){
				if(d[i][j]->id == VIDE){
					texture(d,i,j,TRESOR,difficulte);
					return 0;
				}
			}
		}
	}
	return 1;
}

int leviergen(cell_t * d[N][M], int x_tour, int y_tour, int difficult){
	int lig_ale;
	int col_ale;
	int compteur = 1;
	do{
		col_ale = nbr_aleatoire(y_tour/2, y_tour);
		lig_ale = nbr_aleatoire(1, N - 1);
		if(d[lig_ale][col_ale]->id == VIDE
		&& d[lig_ale-1][col_ale+1]->id != TOUR
		&& d[lig_ale][col_ale-1]->id != TOUR
		&& d[lig_ale+1][col_ale]->id != TOUR
		&& d[lig_ale-1][col_ale]->id != TOUR
		&& d[lig_ale+1][col_ale+1]->id != TOUR
		&& d[lig_ale-1][col_ale-1]->id != TOUR
		&& d[lig_ale+1][col_ale-1]->id != TOUR
		&& d[lig_ale-1][col_ale+1]->id != LEVIER
		&& d[lig_ale][col_ale-1]->id != LEVIER
		&& d[lig_ale+1][col_ale]->id != LEVIER
		&& d[lig_ale-1][col_ale]->id != LEVIER
		&& d[lig_ale+1][col_ale+1]->id != LEVIER
		&& d[lig_ale-1][col_ale-1]->id != LEVIER
		&& d[lig_ale+1][col_ale-1]->id != LEVIER){
			cell_init_levier(&d[lig_ale][col_ale], x_tour,y_tour);
			compteur --;
		}
	}while(compteur > 0);
	return 0;
}

int tourgen(cell_t * d[N][M], int difficult, int compteur){
	int i;
	int j;
	int ale_tour;
	int nbr_tour;
	if(compteur <= 2){
		if(compteur%2 != 0){
			for(i = N - 2; i > 1; i--){
				for(j = M - 5; j > 10; j--){
					if(d[i][j+1]->id == VIDE 
						&& d[i-1][j+1]->id == VIDE
						&& d[i][j-1]->id == VIDE
						&& d[i+1][j]->id == VIDE
						&& d[i-1][j]->id == VIDE
						&& d[i+1][j+1]->id == VIDE
						&& d[i-1][j-1]->id == VIDE
						&& d[i+1][j-1]->id == VIDE){
						
						
						ale_tour = nbr_aleatoire(0,4);
						if(ale_tour == 0){
							texture(d, i, j, TOUR, difficult);
							compteur++;
							leviergen(d, i, j, difficult);
							nbr_tour = tourgen(d, difficult, compteur);
							return nbr_tour;
						}
					}
				}
			}
			return 0;	
		}
		else{
			for(i = 1; i<N - 2; i++){
				for(j = 10; j<M - 5; j++){
					if(d[i][j+1]->id == VIDE 
						&& d[i-1][j+1]->id == VIDE
						&& d[i][j-1]->id == VIDE
						&& d[i+1][j]->id == VIDE
						&& d[i-1][j]->id == VIDE
						&& d[i+1][j+1]->id == VIDE
						&& d[i-1][j-1]->id == VIDE
						&& d[i+1][j-1]->id == VIDE){
						
						ale_tour = nbr_aleatoire(0,4);
						if(ale_tour == 0){
							texture(d, i, j, TOUR, difficult);
							compteur++;
							leviergen(d, i, j, difficult);
							nbr_tour =tourgen(d, difficult, compteur);
							return nbr_tour;
						}
					}
				}
			}
			return 0;
		}
	}
	return 2;
}



int createDungeon_t(cell_t * d[N][M], int difficult){
	yprov = 0;
	xprov = N/2;
	int dir = 1;
	int xsave = -1;
	int compteur_mur = N*M;
	dir = makeCorridor(d,5, 8, dir, difficult);
	
	if(difficult == 1){
		while(yprov < M-1 && compteur_mur > 1400){
			dir = choix_couloir(dir);
			dir = makeCorridor(d, 7, 10, dir,difficult);
			compteur_mur = nbr_mur(d);

			if(yprov >= M-1 && compteur_mur > 1400){
				xsave = xprov;
				dir = 3;
				dir = makeCorridor(d, 7, 10, dir,difficult);
				compteur_mur = nbr_mur(d);
				while(compteur_mur > 1400){
					dir = choix_couloir(dir);
					dir = makeCorridor(d, 7, 10, dir,difficult);
					compteur_mur = nbr_mur(d);
				}
				yprov = M-1;
			}
			
			if(yprov < M-1 && compteur_mur <= 1400){
				while(yprov < M-1){
					dir = choix_couloir(dir);
					dir = makeCorridor(d, 7, 10, dir,difficult);	
				}
			}							
		}
	}
	else{
		if(difficult == 2){
			while(yprov < M-1 && compteur_mur > 1300){
					dir = choix_couloir(dir);
					dir = makeCorridor(d, 5, 10, dir,difficult);
					compteur_mur = nbr_mur(d);

					if(yprov >= M-1 && compteur_mur > 1300){
						xsave = xprov;
						dir = 3;
						dir = makeCorridor(d, 5, 10, dir,difficult);
						compteur_mur = nbr_mur(d);
						while(compteur_mur > 1300){
							dir = choix_couloir(dir);
							dir = makeCorridor(d, 5, 10, dir,difficult);
							compteur_mur = nbr_mur(d);
						}
						yprov = M-1;
					}
					
					if(yprov < M-1 && compteur_mur <= 1300){
						while(yprov < M-1){
							dir = choix_couloir(dir);
							dir = makeCorridor(d, 5, 10, dir,difficult);	
						}
					}							
				}
						
		}
		else{
			if(difficult == 3){
				while(yprov < M-1 && compteur_mur > 1200){
					dir = choix_couloir(dir);
					dir = makeCorridor(d, 1, 10, dir,difficult);
					compteur_mur = nbr_mur(d);

					if(yprov >= M-1 && compteur_mur > 1200){
						xsave = xprov;
						dir = 3;
						dir = makeCorridor(d, 1, 10, dir,difficult);
						compteur_mur = nbr_mur(d);
						while(compteur_mur > 1200){
							dir = choix_couloir(dir);
							dir = makeCorridor(d, 1, 10, dir,difficult);
							compteur_mur = nbr_mur(d);
						}
						yprov = M-1;
					}
					
					if(yprov < M-1 && compteur_mur <= 1200){
						while(yprov < M-1){
							dir = choix_couloir(dir);
							dir = makeCorridor(d, 1, 10, dir,difficult);	
						}
					}							
				}
			}
		}
	}
	int x = xprov;
	int y = M -1;

	if(xsave != -1){
		texture(d, xsave, y, PORTE,difficult);
		keygen(d, xsave, difficult);
		chestgen(d, xsave, difficult);	
	}
	else{
		texture(d, x, y, PORTE,difficult);
		keygen(d, x, difficult);
		chestgen(d, x, difficult);	
	}	
	
	tourgen(d, difficult, 1)	;
	
	return 0;
}


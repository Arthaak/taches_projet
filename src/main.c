#include "jeu.h"
#include "generic_SDL.h"
#include "menu.h"



int main(int argc, char** argv){

	change_to_cwd();

	SDL_Window *window;
	TTF_Font *font[NB_FONTS];

	init_SDL(&window, font);
	SDL_Renderer *render=init_render(window);

	menu(window, font, render);

	for(int i=0;i<NB_FONTS;i++)
		TTF_CloseFont(font[i]);
	TTF_Quit();

	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(render);
	
	SDL_Quit();

	return 0;
}


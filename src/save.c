#include <unistd.h>
#include <SDL2/SDL.h>
#include <string.h>
#include "jeu_architecture.h"
#include "core.h"
#include "save.h"

/** @file save.c
* Comporte les fonctions permettant de sauvegarder/charger un fichier */



void save_cell_value(cell_t *cell, FILE* f) {
	
	fprintf(f, " %i", cell->id);
	switch(cell->id) {
		case ARME:
			fprintf(f, " %i %i", cell->arme->degats, 
													 cell->arme->portee);
			break;
		case LEVIER:
			fprintf(f, " %i %i %i", cell->levier->x_tour, 
															cell->levier->y_tour,
															cell->levier->statut);
			break;
		case PORTE:
			fprintf(f, " %i", cell->porte->statut);
			break;
		case CLEF:
			fprintf(f, " %i %i", cell->clef->x_porte, 
													 cell->clef->y_porte);
			break;
		case TOUR:
			fprintf(f, " %i %i %i", cell->tour->arme->degats,
															cell->tour->arme->portee,
															cell->tour->statut);
			break;
		case TRESOR:
			fprintf(f, " %i", cell->tresor->score);
		default:
			break;
	}
}


int create_save_file(perso_t *perso, cell_t *cell[][M], int diff,
										 monstre_t *lst_mstr, SDL_Window *window) {
	// agit si le fichier existe déjà (ie. si on va l'écraser)
	if(access("../saves/save", W_OK)==0) {
		const SDL_MessageBoxButtonData buttons[2]={ 
			{0,1, "Oui"},
			{0,0, "Non"}	
		};

		const SDL_MessageBoxData info=
								{SDL_MESSAGEBOX_WARNING, window, "Sauvegarder", 
								"Une sauvegarde existe déjà, voulez-vous l'écraser ?",
								2, buttons, NULL};
		int res;
				
		SDL_ShowMessageBox(&info, &res);
		
		if(!res)
			return 0;
	}

	FILE *f=fopen("../saves/save", "w");
	
	if(!f) {
		perror("La sauvegarde n'a pu être créée");
		return 0;
	
	}
	fprintf(f, "%i %i", NB_MSTR, diff);
	fprintf(f, " %i %i %i %i %i 0 %i %i %i %i 0 %i 0 0", perso->x, perso->y, 
													perso->vie, perso->arme->degats, 
													perso->arme->portee, 
													perso->arme->delai, perso->dir_orientation,
													perso->score, perso->mvt_delay, perso->invul_delay);
	for(int i=0; i<NB_MSTR;i++)
		fprintf(f, " %i %i %i %i 0 %i %i 0 %i %i", lst_mstr[i].x, lst_mstr[i].y,
												lst_mstr[i].arme->degats, lst_mstr[i].arme->portee,
												lst_mstr[i].arme->delai,
												lst_mstr[i].mvt_delay, lst_mstr[i].vie,
												lst_mstr[i].dir_orientation);
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)
			save_cell_value(cell[i][j], f);
			
	fclose(f);
	
	return 1;
}

/**** Charge une partie ****/

/** Rentre dans une cellule les données lues dans le fichier de sauvegarde.
* Helper pour load_save_file().
* @param[in] x l'abscisse de la cellule à lire
* @param[in] y l'ordonnée de la cellule à lire 
* @param[in] diff la difficulte de la partie (lue dans le fichier)
* @param[in] f le fichier en cours de lecture
* @param[out] cell_tab la grille de jeu */
void load_cell_file(cell_t* cell_tab[][M], int x, int y, int diff, FILE *f) {
	int id;
	fscanf(f, " %i", &id);
	cell_tab[x][y]=cell_init(id,x,y, diff);
	
	int xp,yp;
	
	switch(id) {
		case ARME:
			fscanf(f, " %i %i",&(cell_tab[x][y]->arme->degats), 
													&(cell_tab[x][y]->arme->portee));
			break;
		case LEVIER:
			fscanf(f, " %i %i",&xp, &yp);
			cell_init_levier(&cell_tab[x][y], xp,yp);
			fscanf(f, " %i", (int*)&(cell_tab[x][y]->levier->statut));
			break;
		case TOUR:
			fscanf(f, " %i %i %i",&(cell_tab[x][y]->tour->arme->degats),
													&(cell_tab[x][y]->tour->arme->portee), 
													(int*)&cell_tab[x][y]->tour->statut);
			break;
		case PORTE:
			fscanf(f, " %i", (int*)&cell_tab[x][y]->porte->statut);
			break;		
		case CLEF:
			fscanf(f, " %i %i",&xp, &yp);
			cell_init_clef(&cell_tab[x][y], xp,yp);
			break;
		case TRESOR:
			fscanf(f, " %i",&(cell_tab[x][y]->tresor->score)); 
		default:
			break;
	}
}


/** Récupère toutes les données contenues dans le fichier de sauvegarde.
* @param[out] perso le personnage jouable à initialiser 
* @param[out] cell_tab la matrice de la map à initialiser
* @param[in] diff la difficulté du jeu à initialiser
* @param[out] lst_mstr le tableau des monstres actifs à initialiser
* @return 0 s'il y a eu un problème, 1 sinon
*/
int load_save_file(perso_t **perso, cell_t *cell_tab[][M], int *diff,
									 monstre_t **lst_mstr) {
	
	FILE *f=fopen("../saves/save", "r");
	
	if(!f) {
		perror("La sauvegarde n'a pu être lue");
		return 0;
	}
	
	*perso=malloc(sizeof(perso_t));
	(*perso)->arme=malloc(sizeof(arme_t));
	(*perso)->arme->projectile=NULL;
	
	
	fscanf(f, "%i %i", &NB_MSTR, diff);
	fscanf(f, " %i %i %i %i %i %i %i %i %i %i %i %i %i %i", 
									&((*perso)->x), &((*perso)->y), &((*perso)->vie), 
									&((*perso)->arme->degats), &((*perso)->arme->portee), 
									&((*perso)->arme->timer), &((*perso)->arme->delai),
									&((*perso)->dir_orientation), &((*perso)->score),
									&((*perso)->mvt_delay),&((*perso)->mvt_timer),
									&((*perso)->invul_delay), &((*perso)->invul_timer),
									&((*perso)->frame_count));
	
	*lst_mstr=malloc(NB_MSTR*sizeof(monstre_t));
	for(int i=0; i<NB_MSTR;i++) {
		(*lst_mstr)[i].arme=malloc(sizeof(arme_t));
		(*lst_mstr)[i].arme->projectile=NULL;
		
		fscanf(f, " %i %i %i %i %i %i %i %i %i %i", &(((*lst_mstr)[i]).x), 
																		 &(((*lst_mstr)[i]).y),
															 			 &(((*lst_mstr)[i]).arme->degats), 
															 			 &(((*lst_mstr)[i]).arme->portee),
															 			 &(((*lst_mstr)[i]).arme->timer),
															 			 &(((*lst_mstr)[i]).arme->delai), 
															 			 &(((*lst_mstr)[i]).mvt_delay),  
															 			 &(((*lst_mstr)[i]).mvt_timer),
																		 &(((*lst_mstr)[i]).vie),
																		 &(((*lst_mstr)[i]).dir_orientation));
	}
	for(int i=0;i<N;i++) {
		for(int j=0;j<M;j++) {
			if(feof(f))
				printf("wtf");
			load_cell_file(cell_tab,i,j, *diff, f);
		}
	}
			if(!feof(f))
				printf("wtf");
	fclose(f);
	return 1;
}

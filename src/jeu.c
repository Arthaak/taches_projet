#include "jeu.h"
#include "jeu_affichage.h"
#include "jeu_input.h"
#include "popup.h"

#include "save.h"
#include "fonction_donjon.h"
#include "generic_SDL.h"

int LARGEUR_MAP=960;
int SPRITE_SIZE=24; ///< Représente la taille en pixels d'une case de la grille et d'une unité 
int HAUTEUR_HUD=52;
int HAUTEUR_MAP=648;
int HAUTEUR_WIN=700;


/** @file jeu.c
* Comporte la boucle de jeu principale
*/

int play(SDL_Window *window, SDL_Renderer *rend, TTF_Font **font, int difficulte, int *score, int *nb_mstr) {

	SDL_Color color_txt={255,255,255};

	perso_t *perso;
	cell_t *cell[N][M];
	list_t *lst_tours=NULL;
	list_t *lst_projs=NULL;
	monstre_t *lst_mstr=NULL;

	if(difficulte) {

		perso=pj_init(difficulte);

		init_donjon(cell, difficulte);
		createDungeon_t(cell, difficulte);
		lst_mstr=monstres_init(difficulte, cell);
	}

	else // initialise les variables, et annule tout en cas d'erreur
		if(!load_save_file(&perso, cell, &difficulte, &lst_mstr))
			return -1;
	
	*nb_mstr=NB_MSTR;
	tours_init(cell, &lst_tours);

	hud_t interface;
	HUD_init(&interface, perso);	

	// buffer nécessaire à draw_HUD()
	int vie_max=perso->vie; 
	// utilisé par input_kb() pour affiner le mouvement
	int input_timer=0;
	// régule le framerate
	int tickrate, tickrate_comp=0;
	// assiste les mouvements de caméra
	int offset;

	SDL_SetRenderDrawColor(rend, 0,0,0,SDL_ALPHA_OPAQUE);

	jeu_tex_t compil_tex=init_textures(rend);
	


	SDL_Event event;
	while (interface.loop==1)	{
	
		input_wrapper(window, &event, &interface);
		
		offset=update_camera(perso, &interface.camera);
		draw_map(rend, &compil_tex, cell, &interface);
		draw_perso(rend, &compil_tex, perso, &interface, offset);
		draw_monstres(rend, &compil_tex, lst_mstr, offset, perso, &interface);
		draw_projs(rend, &compil_tex, lst_projs, offset, perso,&interface);
		draw_HUD(rend, &compil_tex, &color_txt, font, &interface, vie_max, perso); 
		draw_popup(rend, font[1],&color_txt,cell, lst_mstr, &interface);	
		


		if(interface.saved) {
			if(create_save_file(perso, cell, difficulte, lst_mstr, window))
				SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,
																"Sauvegarde réussie",
																"Votre partie a été sauvegardée", window);
			interface.saved=0;
		}
		
		if(!interface.stop) {
			tickrate=SDL_GetTicks();
			
			if(input_kb(&input_timer, perso, cell, lst_mstr, &lst_projs)==PORTE)
				break;
			monstres_ia(lst_mstr, perso, cell, &lst_projs);
			tour_ia(perso, cell, lst_tours, &lst_projs);
			moves_projs(&lst_projs,perso, cell, lst_mstr);
					
				if(perso->vie<=0) {
					interface.loop=0;
				}
			}
			
		SDL_RenderPresent(rend);
			
		// on veut 30 FPS, on attend donc si un cycle a pris moins de 33ms
		// (meilleur compromis fluidité/performances)
		if(tickrate<tickrate_comp)
			SDL_Delay(tickrate_comp-tickrate);
		tickrate_comp=tickrate+1000/FPS;
	}
	
	map_free_mem(cell);
	monstre_free_mem(&lst_mstr, *nb_mstr);
	free_list(&lst_tours,0);
	free_list(&lst_projs,1);
	*score=perso->score;
	pj_free_mem(&perso);
	FREE(interface.vie);

	xprov=N/2;
	yprov=0; // reset des variables globales de création de la map

	SDL_DestroyTexture(compil_tex.tex_pj);
	SDL_DestroyTexture(compil_tex.tex_tileset);
	SDL_DestroyTexture(compil_tex.tex_HUD);

	return interface.loop;
}


int init_jeu(SDL_Window *window, TTF_Font **font, SDL_Renderer *rend, 
						 int difficulte) {


	// Utilisés en toute fin de partie pour calculer les stats
	int score, nbmstr;
	int res_play, choix;

	int timer=time(NULL);	
	NB_MSTR=difficulte*(N+M)/30;
	res_play=play(window,rend,font,difficulte,&score, &nbmstr);
	if(res_play==-1 || res_play==3)
		return res_play;
	choix=draw_end(rend, font, score, res_play, timer,nbmstr);


	return choix;
}

#include <string.h>
#include "popup.h"
#include "generic_SDL.h"
#include "jeu_input.h"

/** @file popup.c
* Comporte les fonctions permettant d'afficher des infobulles à l'écran
*/


int info_cell(cell_t *cell, char* str) {
	
	int nl=3;
	
	if(cell->id==TRESOR) {
		snprintf(str,GUITXTLENGTH,"Trésor\nBonus au score : %i",
						cell->tresor->score);
		nl=3;
	}
	else if(cell->id==ARME) {
		snprintf(str,GUITXTLENGTH,"Arme\nDégâts : %i\nPortée : %i",
						cell->arme->degats, 
						cell->arme->portee);
	}
	else if(cell->id==TOUR) {
		snprintf(str,GUITXTLENGTH,"Tourelle\nDégâts : %i\nPortée : %i\n%s",
						cell->tour->arme->degats, 
						cell->tour->arme->portee, 
						(cell->tour->statut ? "Active" : "Inactive"));
		nl=4;
	}
	else if(cell->id==CLEF) {
		snprintf(str,GUITXTLENGTH,"Clé\nPorte : %i, %i",
						 cell->clef->y_porte,
						 cell->clef->x_porte);
		nl=4;
	}
	else if(cell->id==LEVIER) {
		snprintf(str,GUITXTLENGTH,"Levier\nDésactive la tourelle\nplacée en %i,%i",
						cell->levier->y_tour, 
						cell->levier->x_tour);
		nl=5;
	}
	else {
		strcpy(str, "");
		nl=0;
	}
	return nl;
}

int info_monstre(monstre_t mstr, char* str) {
	snprintf(str,GUITXTLENGTH,"Monstre\nDégâts : %i\nPortée : %i\nVie : %i",
					mstr.arme->degats, 
					mstr.arme->portee, 
					mstr.vie);
	return 4;
}



void draw_popup_map(SDL_Renderer *rend, TTF_Font *font,
										SDL_Color *color_txt, SDL_Rect camera, cell_t *cell[][M], 
										monstre_t *lst_mstr, int indX, int indY) {
	if(indY>=N)
		indY=N-1;
	if(indX>=M)
		indX=M-1;
	int i;
	for(i=0;i<NB_MSTR;i++)	
		if(lst_mstr[i].x==indX && lst_mstr[i].y==indY)
			break;
		
	if((cell[indY][indX]->id==VIDE || cell[indY][indX]->id==MUR) && i==NB_MSTR)
		return;
	
	char str[GUITXTLENGTH];

	int posX=upscaled(indX)-camera.x;
	int posY=upscaled(indY)-camera.y+HAUTEUR_HUD;	
	int nbl;
	
	if(i==NB_MSTR)
		nbl=info_cell(cell[indY][indX], str);
	else
		nbl=info_monstre(lst_mstr[i], str);

	if(!nbl) return;

	if(posY<HAUTEUR_WIN-100)
		posY+=SPRITE_SIZE;
	else
		posY-=100;
	
	if(posX>LARGEUR_MAP-100)
		posX-=100+SPRITE_SIZE;
		
	int w,h;
	TTF_SizeUTF8(font,str,&w,&h);
	SDL_Rect RectPopup=init_rect(posX, posY, w/nbl+10,nbl*h+10);
	SDL_RenderFillRect(rend, &RectPopup);
	
	draw_text_at(rend, color_txt,font,posX+5,posY+5,str, w/nbl+10);
}

int info_HUD_elem(hud_t *interface, char *str, int posX, int posY) {
	
	int i=1;
	if(coord_dans_rect(posX,posY,interface->save)) {
		strcpy(str, "Sauvegarder la partie");
	}
	else if(coord_dans_rect(posX,posY,interface->quit)) {
		strcpy(str, "Abandonner la partie");
	}
	else if(interface->stop && coord_dans_rect(posX,posY,interface->pause)) {
		strcpy(str, "Relancer la partie");
	}
	else if(!interface->stop && coord_dans_rect(posX,posY,interface->pause)) {
		strcpy(str, "Mettre à pause");
	}
	else if(coord_dans_rect(posX,posY,interface->arme)) {
		strcpy(str, "Statistiques de l'arme actuellement portée");
	}
	else if(coord_dans_rect(posX,posY,interface->score)) {
		strcpy(str, "Votre score actuel");
	}
	else if(coord_dans_rect(posX,posY,interface->position)) {
		strcpy(str, "Position sur la carte");
	}
	else if(coord_dans_rect(posX,posY,interface->vie[0])) {
		strcpy(str, "Votre état de santé");
	}
	else if(coord_dans_rect(posX,posY,interface->aide)) {
		strcpy(str, "Prenez la clé puis passez la porte\npour gagner la partie\n"
								"Tuez les monstres en les attaquant et\n"
								"désactivez les tourelles via les leviers"
								"\nDéplacement : ZQSD\nAttaque : A");
		i=4;
	}
	else
		strcpy(str, "");
	
	return i;
}

void draw_popup_HUD(SDL_Renderer *rend, TTF_Font *font, SDL_Color *color_txt,
										hud_t *interface, int posX, int posY) {		
	
	char str[GUITXTLENGTH];


	int i=info_HUD_elem(interface, str, posX, posY);	
	int j=(i==1 ? i : i+1);


	int w,h;
	TTF_SizeUTF8(font,str,&w,&h);
	
	if(posX>LARGEUR_MAP-w/i)
		posX=LARGEUR_MAP-1.2*w/i;
	posY+=HUD_ITEM_SIZE;

	SDL_Rect RectPopup=init_rect(posX, posY, w*1.05/i,h*1.25*j);
	SDL_RenderFillRect(rend, &RectPopup);
	
	draw_text_at(rend, color_txt,font,posX+5,posY+2,str, 1000);
}

void draw_popup(SDL_Renderer *rend, TTF_Font *font,
								SDL_Color *color_txt, cell_t *cell[][M], 
								monstre_t *lst_mstr, hud_t *interface) {		

	
	int posX, posY;
	SDL_GetMouseState(&posX,&posY);

	if(posX > LARGEUR_MAP || posY > HAUTEUR_WIN)
		return;
	if(posY<HAUTEUR_HUD)
		draw_popup_HUD(rend,font,color_txt,interface, posX,posY);
	else {
		int indX=downscaled(posX+interface->camera.x);
		int indY=downscaled(posY-HAUTEUR_HUD+interface->camera.y);
		draw_popup_map(rend,font,color_txt,
									 interface->camera,cell,lst_mstr, indX,indY);
	}
}

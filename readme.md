# Présentation

Ce dépôt représente un projet de fin d'année visant à créer un jeu de type rogue-like.

# Installation

## Unix

* git clone https://gitlab.com/Arthaak/taches_projet.git
* ./install.sh (qui appellera _sudo_)
* make

## Autres 

Non supportés.

# Équipe

* Timothée Girard
* Logan Meilchen
* Maureen Gallois
* Clément Magniez


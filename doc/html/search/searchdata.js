var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvxy",
  1: "achjlmpt",
  2: "cfgjlmprst",
  3: "acdefhiklmnoprstuv",
  4: "acdfhilmnpqrstvxy",
  5: "aclmpt",
  6: "det",
  7: "abcdghilmptv",
  8: "dfghmnpu",
  9: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Énumérations",
  7: "Valeurs énumérées",
  8: "Macros",
  9: "Pages"
};


var searchData=
[
  ['h_5fbresenham_5fsearch',['h_bresenham_search',['../jeu__interactions_8h.html#a8907d6a68aae5c663e009136e4af27a4',1,'h_bresenham_search(int x1, int y1, int x2, int y2, cell_t *cell[][M]):&#160;jeu_interactions.c'],['../jeu__interactions_8c.html#a8907d6a68aae5c663e009136e4af27a4',1,'h_bresenham_search(int x1, int y1, int x2, int y2, cell_t *cell[][M]):&#160;jeu_interactions.c']]],
  ['h_5fhors_5flimites',['h_hors_limites',['../jeu__interactions_8h.html#a16c6e4620f3bca93fce971eef37c2bd7',1,'h_hors_limites(int x, int y):&#160;jeu_interactions.c'],['../jeu__interactions_8c.html#a16c6e4620f3bca93fce971eef37c2bd7',1,'h_hors_limites(int x, int y):&#160;jeu_interactions.c']]],
  ['h_5fpeut_5fattaquer',['h_peut_attaquer',['../jeu__interactions_8h.html#a6bebc03bcd3fbaf7f887fbf9984ef916',1,'h_peut_attaquer(arme_t *arme):&#160;jeu_interactions.c'],['../jeu__interactions_8c.html#a6bebc03bcd3fbaf7f887fbf9984ef916',1,'h_peut_attaquer(arme_t *arme):&#160;jeu_interactions.c']]],
  ['haut',['HAUT',['../core_8h.html#ad495ebbf79eb11c5b78abd894de2b900a5c97701a87d36c8f2c0de80c5865b8e2',1,'core.h']]],
  ['hauteur_5fhud',['HAUTEUR_HUD',['../core_8h.html#a7f6f6a456681f6a9b3d94f6b38663904',1,'HAUTEUR_HUD():&#160;jeu.c'],['../jeu_8c.html#a7f6f6a456681f6a9b3d94f6b38663904',1,'HAUTEUR_HUD():&#160;jeu.c']]],
  ['hauteur_5fmap',['HAUTEUR_MAP',['../core_8h.html#ac7ee8bbc7a5a1dd7133d4bdc05944d80',1,'HAUTEUR_MAP():&#160;jeu.c'],['../jeu_8c.html#ac7ee8bbc7a5a1dd7133d4bdc05944d80',1,'HAUTEUR_MAP():&#160;jeu.c']]],
  ['hauteur_5fwin',['HAUTEUR_WIN',['../core_8h.html#af5fa39f9fc24151bc78cfdcd41f23c01',1,'HAUTEUR_WIN():&#160;jeu.c'],['../jeu_8c.html#af5fa39f9fc24151bc78cfdcd41f23c01',1,'HAUTEUR_WIN():&#160;jeu.c']]],
  ['hud_5finit',['HUD_init',['../jeu__affichage_8h.html#ab9e20f15af0d6b1dac0dfcab52fbad43',1,'HUD_init(hud_t *interface, perso_t *perso):&#160;jeu_affichage.c'],['../jeu__affichage_8c.html#ab9e20f15af0d6b1dac0dfcab52fbad43',1,'HUD_init(hud_t *interface, perso_t *perso):&#160;jeu_affichage.c']]],
  ['hud_5fitem_5fsize',['HUD_ITEM_SIZE',['../core_8h.html#a1787e91ecf14180e053ef60043ad52cb',1,'core.h']]],
  ['hud_5ft',['hud_t',['../structhud__t.html',1,'']]]
];

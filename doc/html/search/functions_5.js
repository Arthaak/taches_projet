var searchData=
[
  ['h_5fbresenham_5fsearch',['h_bresenham_search',['../jeu__interactions_8h.html#a8907d6a68aae5c663e009136e4af27a4',1,'h_bresenham_search(int x1, int y1, int x2, int y2, cell_t *cell[][M]):&#160;jeu_interactions.c'],['../jeu__interactions_8c.html#a8907d6a68aae5c663e009136e4af27a4',1,'h_bresenham_search(int x1, int y1, int x2, int y2, cell_t *cell[][M]):&#160;jeu_interactions.c']]],
  ['h_5fhors_5flimites',['h_hors_limites',['../jeu__interactions_8h.html#a16c6e4620f3bca93fce971eef37c2bd7',1,'h_hors_limites(int x, int y):&#160;jeu_interactions.c'],['../jeu__interactions_8c.html#a16c6e4620f3bca93fce971eef37c2bd7',1,'h_hors_limites(int x, int y):&#160;jeu_interactions.c']]],
  ['h_5fpeut_5fattaquer',['h_peut_attaquer',['../jeu__interactions_8h.html#a6bebc03bcd3fbaf7f887fbf9984ef916',1,'h_peut_attaquer(arme_t *arme):&#160;jeu_interactions.c'],['../jeu__interactions_8c.html#a6bebc03bcd3fbaf7f887fbf9984ef916',1,'h_peut_attaquer(arme_t *arme):&#160;jeu_interactions.c']]],
  ['hud_5finit',['HUD_init',['../jeu__affichage_8h.html#ab9e20f15af0d6b1dac0dfcab52fbad43',1,'HUD_init(hud_t *interface, perso_t *perso):&#160;jeu_affichage.c'],['../jeu__affichage_8c.html#ab9e20f15af0d6b1dac0dfcab52fbad43',1,'HUD_init(hud_t *interface, perso_t *perso):&#160;jeu_affichage.c']]]
];

var searchData=
[
  ['présentation',['Présentation',['../md_readme.html',1,'']]],
  ['pause',['pause',['../structhud__t.html#a5d292541acd12490f3e16c6ab20c474e',1,'hud_t']]],
  ['perso_2ec',['perso.c',['../perso_8c.html',1,'']]],
  ['perso_2eh',['perso.h',['../perso_8h.html',1,'']]],
  ['perso_5fs',['perso_s',['../structperso__s.html',1,'']]],
  ['perso_5ft',['perso_t',['../jeu__architecture_8h.html#a550089442e996a6b42d393992dbc5436',1,'jeu_architecture.h']]],
  ['pj_5fattaque_5fmstr',['pj_attaque_mstr',['../perso_8h.html#a21a87ca63cb9388f6d319f90d6282073',1,'pj_attaque_mstr(perso_t *perso, monstre_t *lst_mstr):&#160;perso.c'],['../perso_8c.html#a21a87ca63cb9388f6d319f90d6282073',1,'pj_attaque_mstr(perso_t *perso, monstre_t *lst_mstr):&#160;perso.c']]],
  ['pj_5faugmente_5fscore',['pj_augmente_score',['../perso_8h.html#aabc8825def8507cbf88b1d2ba7f02deb',1,'pj_augmente_score(perso_t *perso, cell_t *cell):&#160;perso.c'],['../perso_8c.html#aabc8825def8507cbf88b1d2ba7f02deb',1,'pj_augmente_score(perso_t *perso, cell_t *cell):&#160;perso.c']]],
  ['pj_5fcollide',['pj_collide',['../perso_8h.html#a1e7ebbf87ab5937ffc6a4b8b7335ac99',1,'pj_collide(cell_t *cell[][M], perso_t *perso, monstre_t *lst_mstr, list_t **lst_projs):&#160;perso.c'],['../perso_8c.html#a1e7ebbf87ab5937ffc6a4b8b7335ac99',1,'pj_collide(cell_t *cell[][M], perso_t *perso, monstre_t *lst_mstr, list_t **lst_projs):&#160;perso.c']]],
  ['pj_5ffree_5fmem',['pj_free_mem',['../perso_8h.html#ae2a01d174b4d26725b1fe2147d240c32',1,'pj_free_mem(perso_t **perso):&#160;perso.c'],['../perso_8c.html#ae2a01d174b4d26725b1fe2147d240c32',1,'pj_free_mem(perso_t **perso):&#160;perso.c']]],
  ['pj_5finit',['pj_init',['../perso_8h.html#a69c13ff06a214a467547ff2c2751bf26',1,'pj_init(int difficulte):&#160;perso.c'],['../perso_8c.html#a69c13ff06a214a467547ff2c2751bf26',1,'pj_init(int difficulte):&#160;perso.c']]],
  ['pj_5fmarche_5finterac',['pj_marche_interac',['../perso_8h.html#a089377788a823ece7cfead3c32498e33',1,'pj_marche_interac(perso_t *perso, cell_t *cell, cell_t *cell_tab[][M]):&#160;perso.c'],['../perso_8c.html#a089377788a823ece7cfead3c32498e33',1,'pj_marche_interac(perso_t *perso, cell_t *cell, cell_t *cell_tab[][M]):&#160;perso.c']]],
  ['pj_5fmarche_5flevier',['pj_marche_levier',['../perso_8c.html#af0b695031e189eb57d34206df4819c0a',1,'perso.c']]],
  ['pj_5fmove_5fproj',['pj_move_proj',['../perso_8h.html#ab59219028d5a2852fa3f1946885e9fdb',1,'pj_move_proj(perso_t *perso, cell_t *cell[][M], monstre_t *lst_mstr):&#160;perso.c'],['../perso_8c.html#ab59219028d5a2852fa3f1946885e9fdb',1,'pj_move_proj(perso_t *perso, cell_t *cell[][M], monstre_t *lst_mstr):&#160;perso.c']]],
  ['pj_5fmvt_5fdelay',['PJ_MVT_DELAY',['../core_8h.html#a6bb38bd156ee4c6573c04015e3b60f38',1,'core.h']]],
  ['pj_5fouvre_5fporte',['pj_ouvre_porte',['../perso_8h.html#a38d9ad45dba3b41236d74c210d7ba8a1',1,'pj_ouvre_porte(cell_t *cell, cell_t *cell_tab[][M]):&#160;perso.c'],['../perso_8c.html#a38d9ad45dba3b41236d74c210d7ba8a1',1,'pj_ouvre_porte(cell_t *cell, cell_t *cell_tab[][M]):&#160;perso.c']]],
  ['pj_5fpasse_5fporte',['pj_passe_porte',['../perso_8h.html#a63ec95504aea76ffe528430a26153b32',1,'pj_passe_porte(cell_t *cell):&#160;perso.c'],['../perso_8c.html#a63ec95504aea76ffe528430a26153b32',1,'pj_passe_porte(cell_t *cell):&#160;perso.c']]],
  ['pj_5fperd_5fvie',['pj_perd_vie',['../perso_8h.html#a97bc518521394db3e219ec87c2f96c30',1,'pj_perd_vie(perso_t *perso, int degats):&#160;perso.c'],['../perso_8c.html#a97bc518521394db3e219ec87c2f96c30',1,'pj_perd_vie(perso_t *perso, int degats):&#160;perso.c']]],
  ['pj_5ftire_5fproj',['pj_tire_proj',['../perso_8h.html#a8b07c33b3deef475183255879a0bdd54',1,'pj_tire_proj(perso_t *perso):&#160;perso.c'],['../perso_8c.html#a8b07c33b3deef475183255879a0bdd54',1,'pj_tire_proj(perso_t *perso):&#160;perso.c']]],
  ['pj_5ftuer_5fmonstre',['pj_tuer_monstre',['../perso_8h.html#a3ff8236e1b320c9a41e4057618ef4be5',1,'pj_tuer_monstre(monstre_t *lst_mstr, int i, perso_t *perso):&#160;perso.c'],['../perso_8c.html#a3ff8236e1b320c9a41e4057618ef4be5',1,'pj_tuer_monstre(monstre_t *lst_mstr, int i, perso_t *perso):&#160;perso.c']]],
  ['play',['play',['../jeu_8h.html#a835bd1948734fefd01bc8db2f14b69e1',1,'play(SDL_Window *window, SDL_Renderer *rend, TTF_Font **font, int difficulte, int *score, int *nb_mstr):&#160;jeu.c'],['../jeu_8c.html#a835bd1948734fefd01bc8db2f14b69e1',1,'play(SDL_Window *window, SDL_Renderer *rend, TTF_Font **font, int difficulte, int *score, int *nb_mstr):&#160;jeu.c']]],
  ['popup_2ec',['popup.c',['../popup_8c.html',1,'']]],
  ['popup_2eh',['popup.h',['../popup_8h.html',1,'']]],
  ['porte',['porte',['../structcell__s.html#a8249d9c73ed2a64abf5d4a1150dd46d4',1,'cell_s::porte()'],['../core_8h.html#a7aead736a07eaf25623ad7bfa1f0ee2da5fecb6baf7de2a6d107ee433b4c08616',1,'PORTE():&#160;core.h']]],
  ['porte_5fs',['porte_s',['../structporte__s.html',1,'']]],
  ['porte_5ft',['porte_t',['../jeu__architecture_8h.html#a3cceac2c7b4859dd4d68e5de6b7a8f70',1,'jeu_architecture.h']]],
  ['portee',['portee',['../structarme__s.html#a7876da7aa427c0869b346b9d0181d11a',1,'arme_s']]],
  ['position',['position',['../structhud__t.html#a32fe3a25fad78ee23c0ef6c0f1acd571',1,'hud_t']]],
  ['proj',['PROJ',['../core_8h.html#a7aead736a07eaf25623ad7bfa1f0ee2dac15576653c2652f299ca62157c18053e',1,'core.h']]],
  ['proj_5fs',['proj_s',['../structproj__s.html',1,'']]],
  ['proj_5ft',['proj_t',['../jeu__architecture_8h.html#aa8596711c77d7315fc4eeae601a823ab',1,'jeu_architecture.h']]],
  ['projectile',['projectile',['../structarme__s.html#ad3bcb98998799c65b7db108f3458a875',1,'arme_s']]],
  ['projectile_5finit',['projectile_init',['../jeu__architecture_8h.html#ae024c5ff4f4bf8b08026aed77b209f44',1,'projectile_init(int x1, int y1, int x2, int y2, int delai, arme_t *source, list_t **lst_projs):&#160;jeu_architecture.c'],['../jeu__architecture_8c.html#ae024c5ff4f4bf8b08026aed77b209f44',1,'projectile_init(int x1, int y1, int x2, int y2, int delai, arme_t *source, list_t **lst_projs):&#160;jeu_architecture.c']]]
];

#ifndef GENERIC_SDL_H
#define GENERIC_SDL_H
#define NB_FONTS 3
#include <SDL2/SDL.h>
#include <SDL_ttf.h>

/** Crée la fenêtre et initialise les police d'écriture - paramètres prédéfinis.
* @param[out] window la fenêtre qui sera initialisée
* @param[out] font le tableau de polices qui sera généré (tailles 12, 18, 24) */
void init_SDL(SDL_Window** window, TTF_Font** font);


/** Initialise un SDL_Rect selon les valeurs données
* @param[in] x coordonnées en abscisse du rectangle
* @param[in] y coordonnées en ordonnée du rectangle
* @param[in] w largeur du rectangle
* @param[in] h hauteur du rectangle
* @return le rectangle initialisé */
SDL_Rect init_rect(int x, int y, int w, int h);


/** Initialise le renderer du programme.
* @param[in] window la fenêtre du programme
* @return un renderer lié à cette fenêtre
*/
SDL_Renderer* init_render(SDL_Window *window);

/** Affiche du texte à l'écran dans un SDL_Rect donné.
* @param[in] rend le renderer du jeu
* @param[in] color la couleur d'affichage voulue pour ce texte
* @param[in] font la police d'écriture voulue
* @param[in] RectSrc le SDL_Rect où on veut afficher le texte
* @param[in] str le texte
* @param[in] wrapped le nombre de pixels à partir desquels wrap le texte - 0 si on veut une ligne continue
* @return le SDL_Rect où s'affiche le texte, aux (x,y) identiques à ceux de RectSrc et aux largeur/hauteur relatifs à la taille de la chaîne str
*/
SDL_Rect draw_text_in(SDL_Renderer* rend, SDL_Color *color, TTF_Font *font,
							 SDL_Rect RectSrc, char *str, int wrapped);

/** Wrapper pour draw_text_in().
* Prend deux coordonnées au lieu d'un SDL_Rect - voir draw_text_in() */
SDL_Rect draw_text_at(SDL_Renderer* rend, SDL_Color *color, TTF_Font *font,
							 int x,int y, char *str, int wrapped);


/** Wrapper créant une texture initialisée via une surface tirée d'un fichier.
* @param[in] path le chemin d'accès au fichier source
* @param[in] rend le renderer auquel associer la texture
* @return une texture représentant le fichier source indiqué
* */
SDL_Texture* create_texture(char *path, SDL_Renderer *rend);


/** Change (au sein du programme) le répertoire de travail vers celui où est hébergé le jeu.
* Méthode indispensable pour permettre une exécution globale du programme : permet d'ouvrir correctement les divers fichiers depuis n'importe où.
*/ 
void change_to_cwd(void);

#endif

#ifndef MONSTRE_H
#define MONSTRE_H
#include "jeu_architecture.h"
#include "jeu_interactions.h"


/** Initialise une structure monstre_t selon la difficulté.
* @param[in] difficulte la difficulté de la partie définie auparavant
* @param[in] cell la map, une matrice N*M de cell_t 
* @return un monstre_t initialisé
*/
monstre_t monstre_init(int difficulte, cell_t *cell[][M]);	

/** Wrapper pour monstre_init().
* @param[in] difficulte la difficulté de la partie définie auparavant
* @param[in] cell la map, une matrice N*M de cell_t 
* @return un tableau de monstre_t (de taille NB_MSTR) initialisé
* @*/
monstre_t* monstres_init(int difficulte, cell_t* cell[][M]);

/** Indique qu'un monstre est en mesure de bouger.
* @param[in] monstre l'unité testée
* @return 1 si le délai d'attente est passé, 0 sinon */
int monstre_peut_bouger(monstre_t monstre);


/** Gère la collision entre un monstre et son environnement.
* Ne se préoccupe pas de la collision avec le personnage jouable : s'il est à côté d'un monstre, celui-ci l'attaquera sans se déplacer
* @param[in] cell la matrice de cell_t N*M représentant la map
* @param[in] y,x les coordonnées testées (usuellement une case à côté du monstre testé selon sa direction)
* @param[in] lst_mstr le tableau des monstres actifs 
* @return 1 en cas de collision, 0 sinon
*/

int monstre_collide(cell_t *cell[][M],int y, int x, monstre_t *lst_mstr);

/** Détruit le tableau des monstres tel qu'il a été initialisé.
* @param[out] lst_mstr le tableau à libérer 
* @param[in] nb le nombre de monstres en début de partie 
*/
void monstre_free_mem(monstre_t **lst_mstr, int nb);

/** Contrôle l'attaque d'un monstre.
* Si un monstre voit le personnage, il tirera un projectile vers sa position
* Un monstre ne peut pas voir à travers un mur ou une tour
* @param[in,out] monstre le monstre concerné
* @param[in] perso le personnage jouable actif
* @param[in] cell la matrice de cell_t N*M de la map
* @param[in,out] lst_projs la liste des projectiles actifs
* @return 1 si le monstre a attaqué/repéré le personnage jouable, 0 sinon
*/
int monstre_attaque(monstre_t *monstre,perso_t* perso, cell_t* cell[][M], 
										list_t **lst_projs);

/** Fait agir l'ensemble du tableau lst_mstr via monstre_ia()
* @param[in] lst_mstr le tableau des monstres actifs
* @param[in] perso le personnage jouable
* @param[in] cell la matrice de cell_t N*M de la map
* @param[in,out] lst_projs la liste des projectiles actifs
*/

void monstres_ia(monstre_t *lst_mstr, perso_t *perso,
								 cell_t *cell[][M], list_t **lst_projs);


/** Fait agir un monstre en particulier.
Un monstre se déplace de façon aléatoire, sauf s'il est en position d'attaquer le personnage jouable 
* @param[in] lst_mstr le tableau des monstres actifs
* @param[in,out] i la position dans le tableau lst_mstr du monstre concerné
* @param[in] perso le personnage jouable
* @param[in] cell la matrice de cell_t N*M de la map
* @param[in,out] lst_projs la liste des projectiles actifs
*/
void monstre_ia(monstre_t *lst_mstr, int i, perso_t *perso,
											cell_t *cell[][M], list_t **lst_projs);


/** Déplace un monstre d'une unité s'il est en mesure de bouger.
* @param[in] orient valeur de l'énumération dir_orientation, traduisant la direction du monstre
* @param[in] xdelta, ydelta les valeurs desquelles doivent se déplacer le monstre (l'un doit être nul et l'autre égal à 1)
* @param[in] i la position dans le tableau lst_mstr du monstre concerné
* @param[in] lst_mstr le tableau des monstres actifs
* @param[in] perso le personnage jouable
* @param[in] cell la matrice de cell_t N*M de la map
* @param[in,out] lst_projs la liste des projectiles actifs */
void monstre_moves(int orient, int xdelta, int ydelta, int i, 
									 monstre_t *lst_mstr, perso_t *perso,
									 cell_t *cell[][M], list_t **lst_projs);


/** Fait attaquer un monstre s'il repère le personnage. 
Champ de vision : une ligne devant le monstre de longueur équivalente à sa portée.
* @param[in] xdelta, ydelta les valeurs desquelles doivent se déplacer le monstre (l'un doit être nul et l'autre égal à 1)
* @param[in] monstre le monstre concerné
* @param[in] perso le personnage jouable
* @param[in] cell la matrice de cell_t N*M de la map
* @param[in,out] lst_projs la liste des projectiles actifs
* @return 2 si le monstre a la vue bloquée par une cellule (mur/tour) ; 1 si le monstre a tiré un projectile ; 0 sinon */
int monstre_tire_proj(int xdelta, int ydelta, monstre_t *monstre,
											perso_t* perso,cell_t* cell[][M], list_t **lst_projs);



										
#endif

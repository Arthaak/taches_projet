#ifndef MENU_H
#define MENU_H


#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include "jeu_architecture.h"

int recup_coord(SDL_Rect, SDL_Rect, SDL_Rect);
int recup_coord_lvl(SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect);
int lvl_jeu(SDL_Window *, TTF_Font **, SDL_Renderer *);
int menu(SDL_Window *, TTF_Font **, SDL_Renderer *);


#endif


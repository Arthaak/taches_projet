#ifndef ARCHITECTURE_H
#define ARCHITECTURE_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "core.h"
#include "linked_list.h"

typedef struct cell_s cell_t;
typedef struct clef_s clef_t;
typedef struct tresor_s tresor_t;
typedef struct tour_s tour_t;
typedef struct levier_s levier_t;
typedef struct perso_s perso_t;
typedef struct monstre_s monstre_t;
typedef struct arme_s arme_t;
typedef struct proj_s proj_t;
typedef struct porte_s porte_t;

/** Représente un levier que le joueur peut désactiver.
* Désactiver un levier entraîne la désactivation de la tour placée en (x,y) */
struct levier_s {	
	int x_tour;
	int y_tour;
	etat statut;
};

/** Représente une clé que le joueur peut ramasser.
* Ramasser une clé entraîne l'ouverture de la porte placée en (x,y) */
struct clef_s {
	int x_porte;
	int y_porte;
};


/** Représente une porte que le joueur peut ouvrir selon son statut. */
struct porte_s{
	etat statut;
};

/** Représente un élément que le joueur peut utiliser pour augmenter son score de (score). */
struct tresor_s {
	int score;
};

/** Représente une tour qui attaquera le joueur selon les valeurs de son arme et son statut. */
struct tour_s {
	int x,y;
	arme_t *arme;
	etat statut;
};

/** Représente une arme, utilisée par toute unité active. */
struct arme_s {
	int degats;
	int portee;
	int delai; ///< détermine le temps minimal entre deux attaques
	int timer;
	proj_t *projectile;
};



/** Représente une cellule, part de la grille du jeu. */
struct cell_s {
	type id; ///< identifie l'élément utile de l'union
	union {
		clef_t* clef;
		porte_t* porte;
		arme_t* arme;
		tresor_t* tresor;
		tour_t* tour;
		levier_t* levier;
	};
};


/** Représente un personnage contrôlable par le joueur. */
struct perso_s {
	int x,y;
	int mvt_delay; ///< temps minimal entre deux mouvements
	int mvt_timer;
	SDL_Rect display; ///< coordonnées de l'affichage à l'écran du perso_t
	int dir_orientation; ///< direction du perso_t (selon les quatre directions usuelles)
	int vie;
	arme_t *arme;
	int invul_delay; ///< temps minimal entre deux pertes de vie
	int invul_timer;
	int score;
	int frame_count; ///< nombre d'"animations" existantes dans la sprite sheet du personnage
};

/** Représente un ennemi mobile */
struct monstre_s {
	int x,y;
	int mvt_delay; ///< temps minimal entre deux mouvements
	int mvt_timer;
	SDL_Rect display; ///< coordonnées de l'affichage à l'écran du monstre_t
	int dir_orientation; ///< direction du monstre_t (selon les quatre directions usuelles)
	int vie;
	arme_t *arme;
};

/** Représente une attaque portée par une arme */
struct proj_s {
	int x,y; ///< coordonnées de départ
	int mvt_delay; ///< temps minimal entre deux mouvements
	int mvt_timer;
	SDL_Rect display; ///< coordonnées de l'affichage à l'écran du proj_t
	int dir_orientation; ///< direction du proj_t (selon les quatre directions usuelles)
	int degats;
	int xdst,ydst; ///< coordonnées d'arrivée
	arme_t *source;
};


extern int NB_MSTR; ///< Représente le nombre de monstres actifs dans la partie à chaque instant


/* Initialise une arme selon les valeurs données.
* @param[in] degats, portée les dégâts et la portée voulus
* @param[in] delai la latence minimale entre deux attaques voulue
* @return une arme_t initialisée
*/
arme_t* init_arme(int degats, int portee, int delai);



/* Initialise un projectile selon les valeurs données.
* @param[in] x1,y1 les coordonnées de départ du projectile
* @param[in] x2,y2 les cooordonnées d'arrivée
* @param[in] delai la latence minimale entre deux déplacements
* @param[in] source l'arme dont est originaire le projectile
* @param[in] lst_projs un pointeur vers une liste chaînée de proj_t
* @return un proj_t initialisé
*/
proj_t* projectile_init(int x1, int y1, int x2, int y2, int delai,
												arme_t *source, list_t **lst_projs);


/** Initialise une cellule selon une id donnée (= type de la cellule)).
* @param[in] id une valeur dans l'énumération 'type' définissant la cellule 
* @param[in] x,y les coordonnées de la cellule dans une matrice
* @param[in] diff la difficulté choisie pour la partie
* @return la cell_t initialisée
**/ 
cell_t* cell_init(const int id, const int x, const int y, const int diff);



/** Initialise une cellule contenant une clé pour la raccorder à la porte du niveau.
* @param[out] cell la cellule à initialiser
* @param[in] x,y la position de la porte
*/
void cell_init_clef(cell_t **cell, int x, int y);




/** Initialise une cellule contenant une porte.
* @param[out] cell la cellule concernée */
void cell_init_porte(cell_t *cell);
/** Initialise une cellule contenant une arme.
* @param[out] cell la cellule concernée  */

void cell_init_arme(cell_t *cell);



/** Initialise une cellule contenant un trésor.
* @param[out] cell la cellule concernée */
void cell_init_tresor(cell_t *cell);

/** Initialise une cellule contenant une tourelle.
* @param[out] cell la cellule concernée
* @param[in] x,y les coordonnées de la cellule
* @param[in] difficulte la difficulté de la partie */
void cell_init_tour(cell_t *cell, int x, int y, int difficulte);




/** Initialise une cellule contenant un levier pour le raccorder à une tour.
* @param[out] cell la cellule à initialiser
* @param[in] x,y la position de la tour
*/
void cell_init_levier(cell_t **cell, int x, int y);


/** Supprime le contenu d'une cellule et lui donne l'identifiant VIDE/la supprime également.
* @param[out] cell - la cellule concernée
* @param[in] delete : booléen. 1 -> libérer la mémoire allouée à la cellule ; 0 -> en faire une cellule vide
*/ 
void vide_cell(cell_t** cell, int delete);	


/** Initialise une liste chaînée des tourelles présentes sur la map.
* @param[in] cell la matrice N*M dans laquelle chercher des tours
* @param[out] lst_tours un pointeur vers le début d'une liste chaînée
*/
void tours_init(cell_t *cell[][M], list_t **lst_tours);


/** Libère la mémoire allouée à une matrice de cellules.
* @param[in] cell la matrice N*M à détruire
*/
void map_free_mem(cell_t *cell[][M]);

/** Supprime le contenu d'une cellule et lui donne l'identifiant VIDE/la supprime également.
* @param[out] cell - la cellule concernée
* @param[in] delete : booléen. 1 -> libérer la mémoire allouée à la cellule ; 0 -> en faire une cellule vide
*/ 
void vide_cell(cell_t** cell, int delete);	





#endif





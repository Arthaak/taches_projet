#ifndef GENERIC_LIST_H
#define GENERIC_LIST_H

/** Implémentation d'une liste chaînée générique à lien simple */
typedef struct list_s {
	void *data;
	struct list_s *next;
} list_t;


/** Ajoute une node à la liste, ou l'initialise si elle est vide.
* @param[out] head un pointeur vers le début de la liste
* @param[in] data la donnée enregistrée dans la liste
*/
void add_node(list_t **head, void *data);
/** Supprime une liste et/ou les données qu'elle contient.
* On donne ce choix pour gérer les cas où la liste traite des éléments qui peuvent être libérés ailleurs (où la liste comporte alors des pointeurs fous)
* @param[in,out] head un pointeur vers le début de la liste
* @param[in] free_data booléen : 0 -> supprimer les données, 1 -> les conserver
*/
void free_list(list_t **head, int free_data);
/** Supprime un élément donné d'une liste.
* @param[in,out] head un pointeur vers le début de la liste
* @param[in] data l'élément à supprimer 
*
*/ 
void remove_node(list_t **head, void** data);
#endif

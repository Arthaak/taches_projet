#ifndef SAVE_H
#define SAVE_H

/** Sauvegarde les données du jeu dans un fichier txt.
* Données sauvegardées :l'état du perso, de la map et des monstres - les projectiles sont ignorés et les timers réinitialisés
* @param[in] diff la difficulté de la partie 
* @param[in] perso le personnage jouable dont enregistrer le statut
* @param[in] cell la matrice symbolisant la map à sauvegarder
* @param[in] lst_mstr le tableau de monstre_t à enregistrer
* @param[in] window la fenêtre principale du programme
* @return 0 s'il y a eu une erreur, 1 sinon
*/
int create_save_file(perso_t *perso, cell_t *cell[][M], int diff,
										 monstre_t *lst_mstr, SDL_Window *window);

/** Récupère toutes les données contenues dans le fichier de sauvegarde.
* @param[out] perso le personnage jouable à initialiser 
* @param[out] cell_tab la matrice de la map à initialiser
* @param[in] diff la difficulté du jeu à initialiser
* @param[out] lst_mstr le tableau des monstres actifs à initialiser
* @return 0 s'il y a eu un problème, 1 sinon
*/
int load_save_file(perso_t **perso, cell_t *cell_tab[][M], int *diff,
									 monstre_t **lst_mstr);
	
#endif

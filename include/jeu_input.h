#ifndef INPUT_H
#define INPUT_H
#include "jeu_architecture.h"
#include "jeu_affichage.h"



/** Indique qu'un set de coordonnées est dans les limites d'un SDL_Rect.
* @param[in] x,y les deux coordonnées
* @param[in] RectCible le rectangle 
* @return 1 si la position (x,y) est dans RectCible, 0 sinon */
int coord_dans_rect(int x, int y, SDL_Rect RectCible);


/** Déplace le personnage - helper pour input_kb().
* @param[in] timer  permet de limiter le mouvement du personnage lors de frappes rapides - doit être mis à jour via SDL_GetTicks() dans le game loop
* @param[in] orient l'orientation attendue du personnage, définie via kb_input()
* @param[in] xdelta,ydelta le déplacement attendu - l'un doit être nul et l'autre égal à 1
* @param[in,out] perso le personnage jouable mis en place dans le game loop
* @param[in,out] cell la matrice de cellules constituant la map
* @param[in,out] lst_mstr le tableau des unités hostiles du jeu
* @param[in,out] lst_projs la liste des projectiles actifs à l'appel de la fonction
* @return -1 s'il n'y a pas eu déplacement ; une valeur CLEF/TRESOR/LEVIER/PORTE selon l'énumération type s'il y a eu une interaction, ou 0 s'il y a eu déplacement mais pas interaction 
*/
int moves_perso(int *timer, int orient, int xdelta, int ydelta, 
							 perso_t *perso, cell_t *cell[][M], 
							 monstre_t *lst_mstr, list_t **lst_projs);


/** Déplace le personnage selon le statut du clavier à l'appel de la fonction.
* @param[in] timer  permet de limiter le mouvement du personnage lors de frappes rapides - doit être mis à jour via SDL_GetTicks() dans le game loop
* @param[in,out] perso le personnage jouable mis en place dans le game loop
* @param[in,out] cell la matrice de cellules constituant la map
* @param[in,out] lst_mstr le tableau des unités hostiles du jeu
* @param[in,out] lst_projs la liste des projectiles actifs à l'appel de la fonction
* @return voir moves_perso()
*/
int input_kb(int *timer, perso_t *perso, cell_t *cell[][M], 
						 monstre_t* lst_mstr, list_t **lst_projs);


/** Utilitaire pour input_wrapper().
* Contrôle les actions relatives à l'interface - pause, quitter, sauvegarder
* @param[in] event la structure accueillant les événements "clic de souris"
* @param[in,out] interface la structure contenant les éléments de l'interface
*/
void input_hud(SDL_MouseButtonEvent event, hud_t *interface);


/** Contrôle les événements d'interface et de fermeture du jeu durant la partie.
* @param[in] window fenêtre du programme
* @param[in] event événement lié à la souris
* @param[in] interface la structure contenant tous les éléments de l'interface
*/
void input_wrapper(SDL_Window *window, SDL_Event *event, hud_t *interface);


/** Contrôle les décisions prises dans l'écran de fin de partie.
* @param[in] RectQuit,RectContinue,RectBack : trois rectangles dans lesquels un clic affecte la valeur de retour
* @return -1 -> aucun choix n'a été fait<br> 0 -> fermeture de la fenêtre<br> 1 -> recommencer une partie<br> 2 -> retour au menu
*/
int input_end_screen(SDL_Rect RectQuit, SDL_Rect RectContinue, 
										 SDL_Rect RectBack);

#endif

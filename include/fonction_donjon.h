#ifndef __PROV__
#define __PROV__
#include <stdlib.h>
#include <stdio.h>
#include "core.h"
extern int xprov;
extern int yprov;

int makeCorridor(cell_t * donjon[N][M],int minlength,int maxlenght, 
								 int direction, int difficulte); //Met en place un couloir de taille length vers direction

void makeRoom(cell_t * d[N][M],int xlength, int ylength, int direction); //Crée une salle 


int createDungeon_t(cell_t * d[N][M], int difficult);
void init_donjon(cell_t * donjon[N][M], int difficulte);

#endif

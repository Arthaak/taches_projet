#ifndef CORE_H
#define CORE_H

#define PJ_MVT_DELAY 200
#define FREE(k) free(k);k=NULL;


#define N 40 ///< Hauteur de la map (coordonnées logiques)
#define M 50 ///< Largeur de la map (coordonnées logiques)
#define FPS 30 ///< Frames par seconde voulues

#define NB_SPRITES 10 ///< Quantité de sprites dans tileset.png
#define NB_HUD_ITEMS 6 ///< Quantité de sprites dans boutons.png
#define HUD_ITEM_SIZE 30 /// Dimensions des sprites de boutons.png
#define GUITXTLENGTH 200 ///< Nombre maximal de caractères d'un élément des HUD
#define NB_PJ_ANIM 3 ///< Animations de déplacement existantes pour le personnage
#define upscaled(x) (x)*SPRITE_SIZE
#define downscaled(x) (x)/SPRITE_SIZE

#include <SDL2/SDL.h>
#include <SDL_ttf.h>

extern int SPRITE_SIZE;
extern int HAUTEUR_HUD;
extern int HAUTEUR_MAP;
extern int LARGEUR_MAP;
extern int HAUTEUR_WIN;

/** Récapitule les valeurs possibles d'un élément de l'affichage dans l'ordre du tileset.
* Également utilisé (de façon abusive) pour déterminer ce qu'une cell_t peut contenir.
*/ 
typedef enum {VIDE,MUR,CLEF,PORTE,ARME,TOUR,TRESOR,LEVIER,MONSTRE,PROJ} type;

typedef enum {INACTIF,ACTIF} etat;
typedef enum {HAUT,BAS,GAUCHE,DROITE} dir_orientation;



#endif

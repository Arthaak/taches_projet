#ifndef PERSO_H
#define PERSO_H
#include "jeu_interactions.h"


/** Initialise le personnage jouable selon la difficulté choisie et des valeurs prédéfinies.
* @param[in] difficulte la difficulté définie avant lancement de la partie
* @return la structure initialisée
*/
perso_t* pj_init(int difficulte);


/** Retire de la vie au personnage s'il n'est pas en état invulnérable.
* @param[in,out] perso le personnage jouable
* @param[in] degats le montant de points de vie à lui retirer
* @return 1 si la vie du personnage a diminué, 0 sinon
*/
int pj_perd_vie(perso_t *perso, int degats);

/** Contrôle la mort d'un monstre et augmente le score du personnage jouable le cas échéant.
* Concrètement : décale vers la fin du tableau lst_mstr tout monstre mort et décrémente NB_MSTR de sorte à ne plus prendre ces monstres en considération.
* Permet une gestion plus facile de la mémoire allouée au tableau lst_mstr.
* @param[in,out] lst_mstr le tableau des monstres actifs 
* @param[in] i la position dans le tableau du monstre concerné 
* @param[out] perso le personnage jouable*/
void pj_tuer_monstre(monstre_t *lst_mstr, int i, perso_t *perso);

/* Augmente le score du perso quand il marche sur un trésor et supprime celui-ci.
* @param[out] perso le personnage jouable
* @param[in,out] la cellule visée (comportant normalement un trésor)
* @return TRESOR -> réussite, 0 sinon 
*/
int pj_augmente_score(perso_t *perso, cell_t *cell);


/** Ouvre la porte du niveau quand le personnage interagit avec une cellule de type clef.
* @param[in] cell la cellule sujette à l'interaction 
* @param[in] cell_tab le tableau de cellules représentant la map
* @return CLEF -> réussite, 0 sinon */
int pj_ouvre_porte(cell_t *cell, cell_t *cell_tab[][M]);

/** Ouvre la porte du niveau quand le personnage l'atteint si elle est "active" 
* @param[in] cell la cellule sujette à l'interaction 
* @return PORTE -> réussite, 0 sinon */
int pj_passe_porte(cell_t *cell);

/* Gère les collisions du personnage avec les cellules/ennemis/projectiles.
* @param[in] cell le tableau de cellules représentant la map
* @param[in,out] perso le personnage jouable
* @param[in] lst_mstr le tableau des monstres actifs
* @param[in] lst_projs la liste des projectiles actifs
*/
int pj_collide(cell_t *cell[][M], perso_t *perso, monstre_t *lst_mstr, 
							 list_t **lst_projs);



/** Blesse un monstre touché par le projectile du personnage.
* @param[in,out] perso le personnage jouable
* @param[out] lst_mstr le tableau des monstres actifs */
int pj_attaque_mstr(perso_t *perso,	monstre_t *lst_mstr);

/** Tire un projectile selon l'orientation du personnage.
* @param[in,out] perso le personnage jouable
*/
void pj_tire_proj(perso_t* perso);	

/** Déplace le projectile du personnage.
* Se base sur l'algorithme de Bresenham - fonction différente de moves_hostile_projs() notamment sur ses valeurs de retour. 
* @param[in] perso le personnage jouable
* @param[in] cell la matrice de la map
* @param[in,out] lst_mstr le tableau des monstres actifs
* @return : 0 si la trajectoire doit se poursuivre, 1 si elle doit s'arrêter
*/
int pj_move_proj(perso_t *perso, cell_t *cell[][M], monstre_t* lst_mstr);

/** Interagit avec une cellule selon le type de celle-ci.
* @param[in] perso le personnage jouable
* @param[in,out] cell la cellule concernée
* @param[in,out] cell_tab la matrice de la map
* @return 1 en cas d'interaction (réussie ou non), 0 sinon
*/
int pj_marche_interac(perso_t* perso, cell_t* cell, 
											cell_t *cell_tab[][M]);

/** Libère la mémoire prise par une structure perso_t et y assigne NULL.
* @param[in] perso la structure à détruire
*/
void pj_free_mem(perso_t **perso);

#endif

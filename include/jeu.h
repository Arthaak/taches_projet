#ifndef JEU_H
#define JEU_H

#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include "jeu_architecture.h"


/** Fonction principale où opère la boucle du jeu.
* @param[in] window la fenêtre du programme initialisée dans main.c
* @param[in] font le tableau de polices initialisé dans main.c
* @param[in] rend le renderer initialisé dans main.c
* @param difficulte la difficulté choisie dans le menu (0 pour charger une partie)
* @param score le score du perso, utilisé dans draw_end()
* @param nb_mstr prend la valeur du nombre de monstres actifs en début de partie - utilisé dans draw_end()
* @return 0 -> le perso est mort<br> 1 -> partie gagnée<br>2 fermeture du programme (croix rouge)<br> 3 abandon via le bouton des HUD<br>-1 : la sauvegarde n'a pu être chargée
*/
int play(SDL_Window *window, SDL_Renderer *rend, TTF_Font **font, 
				 int difficulte, int *score, int *nb_mstr);


/** Fonction générale appelant la boucle du jeu puis l'écran de fin.
* @param window - la fenêtre du programme initialisée dans main.c
* @param font - la police initialisée dans main.c
* @param rend - le renderer initialisé dans main.c
* @param difficulte - la difficulté choisie dans le menu (0 pour charger une partie)
* @return 0 -> fermeture de la fenêtre ; 1 si l'utilisateur choisit de recommencer une partie ; 4 en cas de retour au menu
*/
int init_jeu(SDL_Window *window, TTF_Font **font, SDL_Renderer *rend, 
						 int difficulte);

#endif


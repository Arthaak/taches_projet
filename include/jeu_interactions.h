#ifndef INTERACTIONS_H
#define INTERACTIONS_H
#include <stdlib.h>
#include "core.h"
#include "jeu_architecture.h"
#include "monstre.h"
#include "perso.h"
#include "linked_list.h"






/** Indique qu'une arme peut être utilisée.
* @param[in] arme l'arme testée
* @return 1 si le délai d'attente est écoulé, 0 sinon */
int h_peut_attaquer(arme_t *arme);


/** Indique qu'un set de coordonnées (x,y) est dans la map.
* @param[in] x,y les coordonnées testées
* @return 1 si les coordonnées sont correctes, 0 sinon */
int h_hors_limites(int x, int y);


/** Indique qu'une case est infranchissable.
* @param[in] cell la cellule testée
* @return 1 si la cellule est bloquante, 0 sinon
*/
int cell_bloquante(cell_t *cell);
/** Indique qu'une tourelle peut attaquer le joueur.
* Implémentation basique de l'algorithme de Bresenham.
* @param[in] x1,y1 origine du déplacement
* @param[in] x2,y2 destination du déplacement
* @param[in] cell matrice N*M de cell_t représentant la map
* @return 1 -> destination atteinte ; 0 -> arrêt par une cellule bloquante
*/
int h_bresenham_search(int x1, int y1, int x2, int y2, cell_t *cell[][M]);
	
	

/** Déplace un projectile tiré par un monstre ou une tourelle.
* Différent de pj_move_proj() en ses valeurs de retour et son comportement quand le projectile croise un monstre ou le personnage jouable.
* @param[in,out] proj le projectile concerné
* @param[out] perso le personnage jouable
* @param[in] cell le tableau représentant la map
* @return 2 si le personnage jouable a été touché ; 1 si le projectile a atteint sa destination sans rien toucher ; 0 sinon
*/
int moves_hostile_projs(proj_t *proj, perso_t *perso, cell_t *cell[][M]);


/** Gère les mécanismes d'attaque d'une tour.
* Une tour va tenter d'atteindre le joueur via l'algorithme de Bresenham s'il est à portée, et le cas échéant tirer un projectile sur cette trajectoire.
* @param[in,out] perso le personnage jouable
* @param[in] cell_tab la matrice de cell_t N*M représentant la map
* @param[in] tour la tour concernée
* @param[out] lst_projs la liste des projectiles actifs, modifiée si la tour en tire un
*/
void tour_attaque(perso_t *perso, cell_t *cell_tab[][M], 
											tour_t *tour, list_t **lst_projs);
											


/** Wrapper pour tour_attaque() qui teste chaque tour l'une après l'autre.
* @param[in,out] perso le personnage jouable
* @param[in] cell_tab la matrice de cell_t N*M représentant la map
* @param[in] lst_tours la liste des tours actives
* @param[out] lst_projs la liste des projectiles actifs, modifiée si la tour en tire un
*/
void tour_ia(perso_t *perso, cell_t *cell_tab[][M], 
								 list_t *lst_tours, list_t **lst_projs);


/** Déplace tous les projectiles existants.
* @param[out] lst_projs la liste des projectiles actifs
* @param[in,out] perso le personnage jouable
* @param[in] cell la matrice de cell_t N*M représentant la map
* @param[in] lst_mstr le tableau des monstres actifs
*/
void moves_projs(list_t **lst_projs, perso_t *perso,
												 cell_t *cell[][M], monstre_t *lst_mstr);
											
#endif

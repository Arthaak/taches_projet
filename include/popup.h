#ifndef POPUP_H
#define POPUP_H

#include "jeu_architecture.h"
#include "jeu_affichage.h"
#include "core.h"


/** Lit le contenu d'une cell_t dans une chaîne de caractères.
* Helper pour draw_popup()
* @param[in] cell la cellule à lire
* @param[out] str la chaîne de caractères où entrer les données
* @return le nombre de sauts de ligne dans str
*/
int info_cell(cell_t *cell, char* str);


/** Lit le contenu d'un monstre_t dans une chaîne de caractères.
* Helper pour draw_popup()
* @param[in] mstr le monstre_t à lire
* @param[out] str la chaîne de caractères où entrer les données
* @return 4 (le nombre de sauts de ligne dans str)
*/
int info_monstre(monstre_t mstr, char* str);

/** Affiche un popup sur la map illustrant les stats des monstres ou des structures.
* @param[in] rend le renderer du jeu
* @param[in] font la police d'écriture utilisée pour le texte du popup
* @param[in] color_txt la couleur de cette police
* @param[in] camera la caméra utilisée dans le jeu
* @param[in] cell la matrice de cellules symbolisant la map
* @param[in] lst_mstr le tableau des monstres actifs en jeu
* @param indX,indY la position de la souris downscaled pour correspondre à sa position logique (dans la matrice cell)
*/
void draw_popup_map(SDL_Renderer *rend, TTF_Font *font,
										SDL_Color *color_txt, SDL_Rect camera, cell_t *cell[][M], 
										monstre_t *lst_mstr, int indX, int indY);
										
/** Remplit une chaîne de caractères de données sur un élément des HUD.
* Helper pour draw_popup_HUD()
* @param[in] interface la structure contenant les HUD
* @param[out] str la chaîne de caractères accueillant les infos
* @param[in] posX, posY la position de la souris
* @return le nombre de lignes dans le popup */

int info_HUD_elem(hud_t *interface, char *str, int posX, int posY);										
/** Affiche une infobulle quant à un élément des HUD.
* @param[in] rend le renderer usuel du programme
* @param[in] font la police choisie pour le texte des popups
* @param[in] color_txt la couleur d'écriture
* @param[in] interface la structure contenant les HUD
* @param[in] posX, posY la position de la souris
*/
void draw_popup_HUD(SDL_Renderer *rend, TTF_Font *font, SDL_Color *color_txt,
										hud_t *interface, int posX, int posY);		


/** Affiche une infobulle au-dessus des structures du jeu.
* Se déclenche quand la souris est sur la zone HUD.
* @param[in] rend le renderer usuel du programme
* @param[in] font la police choisie pour le texte des popups
* @param[in] color_txt la couleur d'écriture
* @param[in] cell le tableau de cell_t représentant la map
* @param lst_mstr le tableau des monstres actifs sur la map
* @param[in] interface l'interface du jeu
*/
void draw_popup(SDL_Renderer *rend, TTF_Font *font,
								SDL_Color *color_txt, cell_t *cell[][M], 
								monstre_t *lst_mstr, hud_t *interface);		
										
										
#endif

#ifndef AFFICHAGE_H
#define AFFICHAGE_H
#include "jeu_interactions.h"
#include "jeu_architecture.h"

/** @file jeu_affichage.h
* comporte toutes les méthodes affichant un élément du jeu à l'écran */



/** Concentre tous les éléments d'interface du jeu et la caméra */
typedef struct {
	int loop;
	int saved;
	int stop;
	SDL_Rect quit;
	SDL_Rect pause;
	SDL_Rect save;
	SDL_Rect score;
	SDL_Rect arme;
	SDL_Rect position;
	SDL_Rect *vie;
	SDL_Rect camera;
	SDL_Rect aide;
} hud_t;
/** Concentre toutes les textures nécessaires au jeu ainsi que les localisations spécifiques d'un élément dans chacune */
typedef struct {
	SDL_Texture *tex_tileset;
	SDL_Texture *tex_pj;
	SDL_Texture *tex_HUD;
	SDL_Rect rect_tile[2][NB_SPRITES];
	SDL_Rect rect_pj[4][NB_PJ_ANIM];
	SDL_Rect rect_HUD[NB_HUD_ITEMS];
} jeu_tex_t;


/** Fournit l'ensemble des sprites du jeu à une structure jeu_tex_t
* @param[in] rend le renderer du jeu 
* @return la structure complète */
jeu_tex_t init_textures(SDL_Renderer *rend);


/** Vérifie qu'un rectangle est dans la fenêtre du jeu.
* @param[in] RectSrc le rectangle testé
* @param[in] camera la caméra utilisée
* @return 1 -> rectangle visible ; 0 -> rectangle invisible
*/
int rect_est_visible(SDL_Rect RectSrc,SDL_Rect camera);

/** Vérifie qu'un sprite doit être dessiné selon le tileset secondaire.
* @param[in] cell la cellule testée
* @return 1 -> sprite "alternatif" ; 0 -> sprite classique */
int est_alt_tile(cell_t* cell);


/** Déplace un rectangle vers un autre.
* Permet une animation fluide en plaçant un rectangle "cible" à la position d'une unité mobile et en déplaçant vers celui-ci le rectangle "modif" utilisé pour dessiner ladite unité.
* @param[in] RectCible les coordonnées à rejoindre
* @param[in] deplacement la valeur en pixels de laquelle déplacer RectModif
* @param[out] RectModif le rectangle physique à déplacer
* @return la distance en pixels parcourue par RectModif 
*/
int moves_display_rect(SDL_Rect *RectCible,int deplacement,SDL_Rect *RectModif);


/** Détermine la partie de la map devant être affichée à l'écran.
* @param[in] camera la caméra - tout ce qui est dans ses limites doit être affiché
* @param[out] x1,y1 les coordonnées logiques (unités dans une matrice) correspondant aux coordonnées physiques (pixel) de la caméra
* @param[out] x2,y2 les coordonnées logiques représentant "l'arrivée" - largeur et hauteur - de la caméra
*/
void init_map_display(SDL_Rect camera,int *x1, int *y1, int *x2, int *y2);

/** Modifie la position d'un SDL_Rect selon l'orientation du personnage jouable.
* Lorsque la caméra se déplace, seul le personnage jouable se déplace avec elle ; cette fonction compense le mouvement pour les autres unités.
* @param[out] display le SDL_Rect à déplacer
* @param[in] offset la valeur de laquelle déplacer display
* @param[in] perso le personnage jouable dont utiliser l'orientation
* @return 1 -> décalage ; 0 -> pas de modification de display
*/
int offset_camera(SDL_Rect *display,int offset, perso_t *perso);


/** Initialise l'interface du jeu selon la position de départ du personnage.
* @param[out] interface la hud_t à initialiser
* @param[in] perso le personnage jouable que va représenter l'interface */

void HUD_init(hud_t* interface,perso_t *perso);

/** Replace la caméra relativement au personnage jouable.
* @param[in] perso le personnage auquel la caméra s'attache
* @param[in,out] camera le rectangle symbolisant la caméra
* @return le déplacement (cf. moves_display_rect()) */
int update_camera(perso_t *perso, SDL_Rect* camera);

/** Affiche le personnage à l'écran (selon sa position relative à la caméra).
* @param[in] rend le renderer du jeu
* @param[in] src l'agglomération des textures du jeu
* @param[in,out] perso le personnage dessiné
* @param[in] interface la structure de l'interface - dont sa caméra
* @param[in] mvt : booléen - 1 -> la caméra a bougé ; 0 -> la caméra est statique
* @return 1 -> personnage visible ; 0 -> hors-champ (ie. bug de caméra) */
int draw_perso(SDL_Renderer* rend,jeu_tex_t *src, 
							 perso_t *perso, hud_t *interface, int mvt);


/** Affiche un écran de fin de partie.
* Récapitule celle-ci selon les données du perso_t instancié dans play() et donne divers choix au joueur
* @param[in] rend le renderer usuel
* @param[in] font un tableau de polices
* @param[in] score le score du personnage à la fin de la partie
* @param[in] statut un booléen indiquant si la fonction est appelée après une victoire, un abandon ou une défaite
* @param[in] timer_debut temps au début de la partie
* @param[in] nb_mstr nombre de monstres au début de la partie
* @return le choix fait - cf. input_end_screen()
*/
int draw_end(SDL_Renderer *rend, TTF_Font **font,
						 int score,int statut, int timer_debut, int nb_mstr); 

/** Dessine le tableau de cellules représentant la map sur la fenêtre.
* @param[in] rend le renderer usuel du programme
* @param[in] src l'agglomération des textures du jeu
* @param[in] cell le tableau de cell_t représentant la map
* @param[in] interface la structure de l'interface - dont sa caméra
*/
void draw_map(SDL_Renderer *rend,jeu_tex_t *src, 
							cell_t *cell[][M],hud_t *interface);


/** Affiche les monstres actifs.
* @param[in] rend le renderer usuel du programme
* @param[in] src l'agglomération des textures du jeu
* @param[in] lst_mstr le tableau des monstres actifs
* @param[in] offset le décalage dans la position du monstre dû à la caméra
* @param[in] perso le personnage jouable
* @param[in] interface la structure de l'interface - dont sa caméra
*/

void draw_monstres(SDL_Renderer *rend, jeu_tex_t *src,monstre_t *lst_mstr,
									 int offset, perso_t *perso, hud_t *interface);


/** Affiche les projectiles actifs.
* @param[in] rend le renderer usuel du programme
* @param[in] src l'agglomération des textures du jeu
* @param[in] lst_projs la liste des projectiles actifs
* @param[in] offset le décalage dans la position du projectile dû à la caméra
* @param[in] perso le personnage contrôlé
* @param[in] interface la structure de l'interface - dont sa caméra
*/
void draw_projs(SDL_Renderer *rend,jeu_tex_t *src, list_t* lst_projs,
								int offset,perso_t *perso,hud_t *interface);


/** Dessine l'interface en jeu.
* @param[in] rend le renderer du jeu
* @param[in] src l'agglomération des textures du jeu
* @param[in] color la couleur de rendering du texte choisie
* @param[in] font le tableau de polices utilisées
* @param[out] interface la hud_t à initialiser
* @param[in] maxhp le nombre de points de vie initial du joueur
* @param[in] perso le personnage jouable que va représenter l'interface */
void draw_HUD(SDL_Renderer *rend,jeu_tex_t *src,SDL_Color *color,TTF_Font **font,
							hud_t *interface,int maxhp,perso_t *perso);




#endif 
